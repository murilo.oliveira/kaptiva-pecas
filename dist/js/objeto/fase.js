var Fase = (function (numero, paginaInicial) {
    this.nome = "Módulo " + numero;
    this.pasta = "fase" + numero;
    this.paginaInicial = paginaInicial;
    this.numeroFase = null;
    this.paginaFinal = null;
    this.etapas = [];
    this.status = null;
    this.pago = null;
    this.id = '';
    this.pago = 0;
    this.status = 0;
    this.porcentagem = 0;

    this.setNome = function (nome) {
        this.nome = nome;
    };

    this.getNome = function () {
        return this.nome;
    };

    this.setPasta = function (pasta) {
        this.pasta = pasta;
    };

    this.getPasta = function () {
        return this.pasta;
    };

    this.setStatus = function (status) {
        this.status = status;
    };

    this.getStatus = function () {
        return this.status;
    };

    this.setEtapas = function (etapas) {
        this.etapas = etapas;
    };

    this.getEtapas = function () {
        return this.etapas;
    };

    this.addEtapa = function (etapa) {
        this.etapas[this.etapas.length] = etapa;
    };

    this.getPorcentagem = function () {
        var atual = 0;
        for (var i = 0; i < this.etapas.length; i++) {
            atual += this.etapas[i].getPorcentagem();
        }
        if (this.porcentagem === 100) return 100;
        this.porcentagem = Math.round(atual / this.etapas.length);
        if (this.porcentagem === 100) this.status = 1;
        return this.porcentagem;
    };

    this.getPorcentagemAntigo = function () {
        var atual = 0;
        for (var i = 0; i < this.etapas.length; i++) {
            if (this.etapas[i].getStatus() === 1) {
                atual++;
            }
        }
        if (this.porcentagem === 100) return 100;
        this.porcentagem = Math.round((atual / this.etapas.length) * 100);
        if (this.porcentagem === 100) this.status = 1;
        return this.porcentagem;
    };

    this.getPorcentagemVoltaLMS = function (atual) {
        if (atual === 0) return 0;
        this.porcentagem = Math.round(atual / this.etapas.length);
        if (this.porcentagem === 100) this.status = 1;
        return this.porcentagem;
    };

    this.getNumeroFase = function () {
        return this.numeroFase;
    };

    this.setNumeroFase = function (numeroFase) {
        this.pasta = numeroFase;
    };

    this.setPaginaFinal = function () {
        this.paginaFinal = this.etapas[this.etapas.length - 1].getPaginaFinal();
    };

    this.getPaginaFinal = function () {
        return this.paginaFinal;
    };

    this.setPaginaInicial = function (_pg) {
        this.paginaInicial = _pg;
    };

    this.getPaginaInicial = function () {
        return this.paginaInicial;
    };

    this.isCompleto = function () {

        var completo = true;
        for (var i = 0; i < this.etapas.length; i++) {
            if (!this.etapas[i].isCompleto())
                completo = false;
        }
        return completo;

    };

    this.isInRange = function (pagina) {
        var retorno = null;
        // Retorno
        // -1 o id atual é menor que a menor pagina
        // 0 o id atual está neste objeto
        // 1 o id atual está em um objeto maior
        if (pagina < this.getPaginaInicial()) {
            return -1;
        } else if (this.getPaginaInicial() <= pagina && pagina <= this.getPaginaFinal()) {
            return 0;
        } else {
            return 1;
        }

    };

    this.reconstruirCurso = function (id) {
        var atual = 0;
        var etapasL = this.etapas.length;
        for (var i = 0; i < etapasL; i++) {
            atual += this.etapas[i].reconstruirCurso(id);
            _console('\t\tetapa ' + i + ': '  + this.etapas[i].isCompleto() + ' | pags: [' +
                this.etapas[i].getPaginaInicial() + ' ~ ' + this.etapas[i].getPaginaFinal() + '] ' +
                this.etapas[i].porcentagem + '%');
        }
        return this.getPorcentagemVoltaLMS(atual);
    };
});