var Curso = (function (nome) {
    this.nome = nome;
    this.fases = [];
    this.porcentagem = 0;
    this.desafio = null;
    this.totalPaginas = 0;
    this.paginaFinal = null;
    this.status = 0;

    this.setNome = function (nome) {
        this.nome = nome;
    };

    this.getNome = function () {
        return this.nome;
    };

    this.setFases = function (fases) {
        this.fases = fases;
    };

    this.getFases = function () {
        return this.fases;
    };


    this.addFase = function (fases) {
        this.fases[this.fases.length] = fases;
    };

    this.setDesafio = function (desafio) {
        this.desafio = desafio;
    };

    this.getDesafio = function () {
        return this.desafio;
    };

    this.getPorcentagem = function () {
        var soma = 0;
        for (var i = 0; i < this.fases.length; i++) {
            soma += this.fases[i].getPorcentagem();
        }
        this.porcentagem = soma / this.fases.length;
        if (this.porcentagem === 100) this.status = 1;
        return soma / this.fases.length;
    };

    this.getPorcentagemVoltaLMS = function(atual) {
        if (atual === 0) return 0;
        this.porcentagem = Math.round(atual / this.fases.length);
        if (this.porcentagem === 100) this.status = 1;
        return atual / this.fases.length;
    };

    this.calcularPaginas = function () {
        var total;

        // itera pra cada fase.
        for (var i = 0, lengthF = this.fases.length; i < lengthF; i++) {
            var fase = this.fases[i];
            fase.id = 'f'+ i;

            // itera pra cada etapa da fase atual.
            for (var j = 0, lengthE = fase.getEtapas().length; j < lengthE; j++) {
                var etapa = fase.getEtapas()[j];
                etapa.id = 'f' + i + 'e' + j;
                total = etapa.getPaginaInicial() - 1;

                // itera para cada objeto da etapa atual.
                for(var k = 0,length = etapa.objetos.length; k < length; k++) {
                    etapa.objetos[k].setPaginaFinal(total);
                    etapa.objetos[k].setPaginaInicial(total + 1);
                    etapa.objetos[k].id = etapa.id + 'o' + k;
                    total += etapa.objetos[k].getQtdPagina();
                    controladorDeObjetos.push(new controleObjetos(i, j, k, etapa.objetos[k]));
                }
                etapa.setPaginaFinal(total);
            }
            fase.setPaginaFinal();
        }
        this.setPaginaFinal();
    };

    this.calcularPaginas2 = function () {
        var atual = 0;
        // itera pra cada fase.
        for (var i = 0, lengthF = this.fases.length; i < lengthF; i++) {
            var fase = this.fases[i];
            fase.id = 'f'+ i;
            // itera pra cada etapa da fase atual.
            for (var j = 0, lengthE = fase.getEtapas().length; j < lengthE; j++) {
                var etapa = fase.getEtapas()[j];
                etapa.id = 'f' + i + 'e' + j;
                etapa.setPaginaFinalImplicito();
                etapa.setQtdPagina(etapa.getPaginaFinal() - etapa.getPaginaInicial() + 1);
            }
            fase.setPaginaFinal();
        }
        this.setPaginaFinal();
    };

    this.setPaginaFinal = function () {
        this.paginaFinal = this.fases[this.fases.length - 1].getPaginaFinal();
    };

    this.getPaginaFinal = function () {
        return this.paginaFinal;
    };

    this.reconstruirCurso = function (id) {
        var atual = 0;
        var fasesL = this.fases.length;
        for (var i = 0; i < fasesL; i++) {
            atual += this.fases[i].reconstruirCurso(id);
            _console('\tfase ' + i + ': '  + this.fases[i].isCompleto() + ' | pags: [' +
                this.fases[i].getPaginaInicial() + ' ~ ' + this.fases[i].getPaginaFinal() + '] ' +
                this.fases[i].porcentagem + '%');
        }
        this.getPorcentagemVoltaLMS(atual);
        _console('curso: '  + this.status + ' | porcentagem: ' + this.porcentagem + '%');
    };
});