(function () {
    'use strict';
    var module = {
        ratio: 2.19,
        init: function (id) {
            var me = this;
            if (document.addEventListener) {
                this.el = document.getElementById(id);
                this.resize();
                this.plugins();
                window.addEventListener('resize', function (e) {
                    console.log("ola")
                    var size = me.resize();
                    $(me.el).turn('size', size.width, size.height);
                });
            }
        },
        resize: function () {
            this.el.style.width = '';
            this.el.style.height = '';
            var width = this.el.clientWidth,
                height = Math.round(width / this.ratio),
                padded = Math.round(document.body.clientHeight * 0.9);
            if (height > padded) {
                height = padded;
                width = Math.round(height * this.ratio);
            }
            this.el.style.width = width + 'px';
            this.el.style.height = height + 'px';
            return {
                width: width,
                height: height
            };
        },
        plugins: function () {
            $(this.el).turn({
                gradients: true,
                acceleration: true,
                autoCenter:true,
            });
            document.body.className = 'hide-overflow';
        }
    };
    module.init('magazine');
}());
(function () {
    'use strict';


    // $('.magazine').turn({
    //     width: 1200,
    //     height:760,
    //     gradients: true,
    //     acceleration: false,
    //     autoCenter: true,
    //
    // });
    // var flipbookEL = document.getElementById('magazine');
    //
    // window.addEventListener('resize', function (e) {
    //     flipbookEL.style.width = '';
    //     flipbookEL.style.height = '';
    //     $(".magazine").turn('size', flipbookEL.clientWidth, flipbookEL.clientHeight);
    // });


    $('.magazine-viewport').zoom({
        flipbook: $('.magazine'),
        when: {

            max: function () {

                return largeMagazineWidth() / $('.magazine').width();

            },

            swipeLeft: function () {

                $(this).zoom('flipbook').turn('next');

            },

            swipeRight: function () {

                $(this).zoom('flipbook').turn('previous');

            },

            resize: function (event, scale, page, pageElement) {
                console.log("Resize");
                // if (scale == 1)
                //     loadSmallPage(page, pageElement);
                // else
                //     loadLargePage(page, pageElement);

            },

            zoomIn: function () {

                let _pages = document.getElementsByClassName("page");
                for (let i = 0; i < _pages.length; i++) {
                    _pages[i].classList.remove("zoom-out");
                    _pages[i].classList.add("zoom-in");
                }

                console.log("zoomIn");

            },

            zoomOut: function () {
                console.log("zoomOUT");
                let _pages = document.getElementsByClassName("page");
                for (let i = 0; i < _pages.length; i++) {
                    _pages[i].classList.remove("zoom-in");
                    _pages[i].classList.add("zoom-out");
                }


            }
        }
    });

}());

function zoomTo(event) {

    if ($('#magazine').turn('page') === 1) return;
    setTimeout(function () {


        if ($('.magazine-viewport').data().regionClicked) {
            $('.magazine-viewport').data().regionClicked = false;
        } else {

            if ($('.magazine-viewport').zoom('value') == 1) {

                $('.magazine-viewport').zoom('zoomIn', event);
            } else {
                $('.magazine-viewport').zoom('zoomOut');
            }
        }
    }, 1);
}


$('#magazine').hover(function () {
    $(this).data('hover', 1); // mouse is over the div
}, function () {
    $(this).data('hover', 0); // mouse is no longer over the div
});

// $(document).mousewheel(function (event, delta, deltaX, deltaY) {
//     if ($('#magazine').data('hover') == 1) {
//         if (deltaY > 0) {
//             $('#magazine').turn('next');
//         } else {
//             $('#magazine').turn('previous');
//         }
//     }
// });
$(window).bind('keydown', function (e) {

    if (e.keyCode == 37)
        $('#magazine').turn('previous');
    else if (e.keyCode == 39)
        $('#magazine').turn('next');

});
// Zoom event

if ($.isTouch)


    $('.magazine-viewport').bind('zoom.doubleTap', zoomTo);
else

    $('.magazine-viewport').bind('zoom.tap', zoomTo);


