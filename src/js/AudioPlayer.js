// (function (window, undefined) {

'use strict';

var AudioPlayer = (function (elemento, arquivo) {
    
    // Player vars
    // settings
    this.settings = {
        volume: 1,
        autoPlay: false,
        notification: true,
        playList: []
    };
    this.context = new AudioContext();
    this.circle = '';// = player.childNodes.item("timeLine");
    this.radius = '';// = circle.r.baseVal.value;
    this.circumference = '';// = radius * 2 * Math.PI;
    this.player = document.getElementById(elemento);
    this.pause = '';
    this._audio = '';
    
    
    this.init = function (options, id, _autoPlay) {
        //debugger;
        this.settings.autoPlay = _autoPlay;
        var _player = this.player;
        var playBtn;
        var pauseBtn;
        var prevBtn;
        var nextBtn;
        var plBtn;
        var repeatBtn;
        var volumeBtn;
        var volumeBtnMute;
        var progressBar;
        var preloadBar;
        var curTime;
        var durTime;
        var trackTitle;
        // var audio;
        var index = 0;
        var volumeBar;
        var volumeLength;
        var repeating = false;
        var seeking = false;
        var rightClick = false;
        var apActive = false;
        // playlist vars
        var file = arquivo[id];
        var pl;
        var plLi;
        var circle = 0;
        var bloqueado = true;
        
        var _id = id;
        //var file = options;
        if (!('classList' in document.documentElement)) {
            return false;
        }
        
        if (this.apActive || this.player === null) {
            return;
        }
        //var options = arquivo;
        // var _settings = this.extend(this.settings, options);
        
        var circle = this.player.querySelector("#timeLine");
        var radius = circle.r.baseVal.value;
        var circumference = radius * 2 * Math.PI;
        circle.style.strokeDasharray = `${circumference} ${circumference}`;
        circle.style.strokeDashoffset = circumference + '';
        
        var offset = 0;
        
        
        // get player elements
        playBtn = this.player.querySelector('#play');
        pauseBtn = this.player.querySelector('#pause');
        volumeBtn = this.player.querySelector('#volumeOK');
        volumeBtnMute = this.player.querySelector('#volume_MUTE');
        progressBar = this.player.querySelector("#bg_timeLine");
        curTime = this.player.querySelector("#tempoCorrido");
        
        // Create audio object
        var audio = new Audio();
        this._audio = audio;
        
        audio.volume = this.settings.volume;
        audio.src = options[_id];
        audio.preload = 'auto';
        
        audio.addEventListener('timeupdate', update, false);
        audio.addEventListener('ended', doEnd, false);
        
        playBtn.addEventListener('click', playToggle, false);
        pauseBtn.addEventListener('click', playToggle, false);
        volumeBtn.addEventListener('click', volumeToggle, false);
        volumeBtnMute.addEventListener('click', volumeToggle, false);
        progressBar.addEventListener('click', clickTimeline, false);
        
        
        this.apActive = true;
        
        if (this.settings.autoPlay) {
           
            playToggle();
            audio.play();
            playBtn.classList.add('playing');
            atualizaPlayer();
            // plLi[index].classList.add('pl-current');
        }
        
        /**
         *  Player methods
         */
        
        function play() {
            if (bloqueado){
                atualizaPlayer();
                
                audio.play();
                playBtn.classList.add('playing');
            }
        }
    
        this.pause = function () {
            if (bloqueado){
                atualizaPlayer();
                audio.pause();
                playerReset();
            }
        };
        
        function atualizaPlayer() {
            audioPlayerAtual = audio;
        }
        
        function playToggle() {
            // for (var i = 0; i < apAtivos.length; i++) {
            //     if (apAtivos[i]) apAtivos[i].pause();
            // }
            if (audio.paused) {
                    console.log('play');
                    play();
                    pauseBtn.classList.remove('escondido');
                    playBtn.classList.add('escondido');
                    bloqueado = false;
    
                setTimeout(
                    function () {
                        bloqueado = true;
                    }, 60
                );
               
                
            } else {
                console.log('pause');
                audio.pause();
                pauseBtn.classList.add('escondido');
                playBtn.classList.remove('escondido');
                setTimeout(
                    function () {
                        bloqueado = true;
                    }, 60
                );
            }
            
        }
        
        
        function volumeToggle() {
            
            if (audio.muted) {
                
                audio.muted = false;
                volumeBtn.classList.remove("escondido");
                volumeBtnMute.classList.add('escondido');
                
            } else {
                audio.muted = true;
                volumeBtn.classList.add("escondido");
                volumeBtnMute.classList.remove('escondido');
            }
        }
        
        
        function update() {
            if (audio.readyState === 0) return;
            
            var barlength = Math.round(audio.currentTime * (100 / audio.duration));
            
            setProgress(barlength);
            var
                curMins = Math.floor(audio.currentTime / 60),
                curSecs = Math.floor(audio.currentTime - curMins * 60),
                mins = Math.floor(audio.duration / 60),
                secs = Math.floor(audio.duration - mins * 60);
            (curSecs < 10) && (curSecs = '0' + curSecs);
            (secs < 10) && (secs = '0' + secs);
            
            curTime.innerHTML = curMins + ':' + curSecs + " / " + mins + ':' + secs;
            
            //console.log(_player.id);
    
            var isElementInView = Utils.isElementInView($("#"+_player.id), false);

            if (isElementInView) {
                //console.log('in view');
            } else {
                audio.pause();
                //console.log('out of view');
            }
            
        }
        
        function doEnd() {
            
            if (!repeating) {
                audio.pause();
                //plActive();
                playBtn.classList.remove('playing');
                return;
            } else {
                this.index = 0;
                play();
            }
            
        }
        
        var center;
        var audioDuration;
        // var angle;
        
        
        function clickTimeline(evt) {
            center = {
                x: progressBar.getBoundingClientRect().width / 2,
                y: progressBar.getBoundingClientRect().height / 2
            }
    
            var obj = progressBar;
            var rect = obj.getBoundingClientRect();
            var difX = _player.offsetWidth - rect.width;
            var difY = _player.offsetWidth - rect.height;
            var calculoOnePageY = window.innerHeight * parseInt(progressBar.offset().top / window.innerHeight, 10) //Calcula o height das paginas
            var calculoOnePageX = window.innerWidth * parseInt(progressBar.offset().left / window.innerWidth, 10) //Calcula o width das paginas
            var x = evt.clientX - progressBar.offset().left - calculoOnePageX;
            var y = evt.clientY - (progressBar.offset().top - calculoOnePageY);
            console.log(x, y)
            var _angle = calculateAngle(x - center.x, center.y - y);
            setTime(_angle);
            setTimeline(_angle);
        }
        
        function getDuration() {
            return audio.duration;
        }
        
        function setTimeline(angle) {
            circle.style.strokeDashoffset = 780.384 - (angle * 2.167733333);
        }
        
        function setTime(angle) {
            //debugger;
            
            var fracaoAudio = getDuration() / 780;
            audio.currentTime = fracaoAudio * (angle * 2.167733333);
        }
        
        function setProgress(percent) {
            offset = circumference - percent / 100 * circumference;
            circle.style.strokeDashoffset = offset;
        }
        
        function calculateAngle(x, y) {
            var k = Math.abs(y) / Math.abs(x);
            var angle = Math.atan(k) * 180 / Math.PI;
            if (y * x > 0) {
                angle = 90 - angle + (y < 0 ? 180 : 0);
            } else {
                angle = angle + (y < 0 ? 90 : 270);
            }
            
            return angle;
        }
    
        this.destroy = function () {
            //debugger;
            // if (!this.apActive) return;
        
            if(!this._audio.paused) this._audio.pause();
            this.apActive = false;
            playerReset();
            playBtn.removeEventListener('click', playToggle, false);
            pauseBtn.removeEventListener('click', playToggle, false);
            volumeBtn.removeEventListener('click', volumeToggle, false);
            audio.removeEventListener('timeupdate', update, false);
            audio.removeEventListener('ended', doEnd, false);
            playBtn = null;
            pauseBtn = null;
            volumeBtn = null;
            audioPlayerAtual = null;
            audio = null;
            
           
        };
        
        function playerReset () {
            if (!pauseBtn.classList.contains('escondido')) pauseBtn.classList.add('escondido');
            if (playBtn.classList.contains('escondido')) playBtn.classList.remove('escondido');
        }
    };
    
    
    
    
    this.moveBar = function (evt, el, dir) {
        var value;
        if (dir === 'horizontal') {
            value = Math.round(((evt.clientX - el.offset().left) + window.pageXOffset) * 100 / el.parentNode.offsetWidth);
            el.style.width = value + '%';
            return value;
        } else {
            var offset = (el.offset().top + el.offsetHeight) - window.pageYOffset;
            value = Math.round((offset - evt.clientY));
            if (value > 100) value = 100;
            if (value < 0) value = 0;
            this.volumeBar.style.height = value + '%';
            return value;
        }
    };
    
    this.handlerBar = function (evt) {
        this.rightClick = (evt.which === 3) ? true : false;
        this.seeking = true;
        this.seek(evt);
    };
    
    this.handlerVol = function (evt) {
        this.rightClick = (evt.which === 3) ? true : false;
        this.seeking = true;
        this.setVolume(evt);
    };
    
    this.seek = function (evt) {
        if (this.seeking && this.rightClick === false && this.audio.readyState !== 0) {
            var value = this.moveBar(evt, this.progressBar, 'horizontal');
            this.audio.currentTime = this.audio.duration * (value / 100);
        }
    };
    
    this.seekingFalse = function () {
        this.seeking = false;
    };
    
    this.notify = function (title, attr) {
        if (!this.settings.notification) {
            return;
        }
        if (window.Notification === undefined) {
            return;
        }
        window.Notification.requestPermission(function (access) {
            if (access === 'granted') {
                var notice = new Notification(title.substr(0, 110), attr);
                notice.onshow = function () {
                    setTimeout(function () {
                        notice.close();
                    }, 5000);
                }
            }
        })
    };
    

    
    this.create = function (el, attr) {
        var element = document.createElement(el);
        if (attr) {
            for (var name in attr) {
                if (element[name] !== undefined) {
                    element[name] = attr[name];
                }
            }
        }
        return element;
    };
    
    Element.prototype.offset = function () {
        var el = this.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        
        return {
            top: el.top + scrollTop,
            left: el.left + scrollLeft
        };
    };
    
    Element.prototype.css = function (attr) {
        if (typeof attr === 'string') {
            return getComputedStyle(this, '')[attr];
        } else if (typeof attr === 'object') {
            for (var name in attr) {
                if (this.style[name] !== undefined) {
                    this.style[name] = attr[name];
                }
            }
        }
    };
    
    var _arquivo = arquivo;
    
    this.init(_arquivo,0, false);
    
    
    // função para chamada de mais de um podcast no mesmo player
    this.playExterno = function (id) {
       
        this.destroy();
        this.init(_arquivo,(id-1), true);
    }
});

