// Script com respostas para atividades da Fase
//debugger;
var msgPreenchaTodasLacunas = "Preencha corretamente as lacunas, para finalizar est\u00e1 atividade.";
var msgParabens = "Resposta correta!";
var msgError = "Resposta incorreta. Retorne ao estudo deste desafio e refa\u00e7a o exerc\u00edcio.";
var msgSelecioneUmaOpcao = "Selecione uma op\u00e7\u00e3o.";
var msgMarqueTodasAsAlternativas = "Marque todas as alternativas corretas para finalizar essa atividade.";
var msgSselecionouAlternativaErrada = "Selecione corretamente todos os campos, para finalizar est\u00e1 atividade.";
var msgParabensFinal = msgParabens + " Acesse o menu de t\u00F3picos do curso e inicie a pr\u00F3xima etapa de estudo.";
var msgPadrao = "Parabéns, você completou a atividade com sucesso!";
var msgFeedback = [];

// ];


function msgNum(formNum, tipo) {
    console.log(formNum, tipo)
    return msgFeedback[formNum][tipo];
}

var loadHTML = [];

function carregouHTML(id) {

    loadHTML.push(1);
    var _porcento = Number.parseInt((loadHTML.length * 100) / 10); // Calcula porcentagem geral das paginas carregadas

    //Horizontal
    var porcentagemAcertos = document.querySelectorAll('.porcentagem0');
    porcentagemAcertos[0].attributes.offset.value = _porcento + '%';

    if (loadHTML.length >= 10) {

        ativarSanfonas();
        ativarBotoes();
        setVideos();
        setTimeout(function () {
            addMaozinhaSVG();
        }, 1000);
        setTimeout(function () {
            $('#carregando').hide();
            $.scrollify();
        }, 1000);


    }
}

function recarregarRecursos(_id) {
    //debugger;
    _controleTotalPaginas++;
    loadHTML.push(1);
    console.log(loadHTML.length)
    var _diferencaPaginas = _paginas - _id;

    if (_diferencaPaginas >= 5 && _controleTotalPaginas == 5) {
        setRecursos();
    } else if (_diferencaPaginas == 0) {
        setRecursos();
    }


}

function setRecursos(_id) {
// debugger;
    ativarSanfonas();
    ativarBotoes();
    setVideos();
    setTimeout(function () {
        removerMazinha();
        setTimeout(function () {
            addMaozinhaSVG();
        }, 1000);
    }, 1000);
    setTimeout(function () {
        $.scrollify();
    }, 1000);
    _cursoCarregado = true;

}