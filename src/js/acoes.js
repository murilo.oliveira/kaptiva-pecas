loadPage();
//
var idAtual = 1;
var idOpcoes = 0; // SE VARIÁVEL ESTIVER EM 1 OPÇÕES DE ACESSIBILIDADE ESTÃO LIGADAS
var limite = 0;
var idUltimoAcesso = 0;
var zoom = 0;
var firefox = /Firefox/i.test(navigator.userAgent);
var ie = /(MSIE|Trident\/|Edge\/)/i.test(navigator.userAgent);
var navegacaoLinear = 1; // SE 0 DESABILITA NAVEGAÇÃO LINEAR. SE 1 HABILITA NAVEGAÇÃO LINEAR.
var zoom = 0;
var contraste = 0;
var fonte = 0;
var bloquearTeclado = 1; // BLOQUEAR TECLADO NA INICIALIZAÇÃO. SE 1 BLOQUEADO. SE 0 DESBLOQUEADO.
var pontosEncontrados360 = 7;
var aulaAtual = 0;
var ultimoVisitado = 0;
var ultimaAulaVisitada = 0;
var ultimoIdVisitado = 0;
var totalProgressLocation = [[0], [0], [0], [0], [0], [0], [0]];
var suspendData = doLMSGetValue("cmi.suspend_data");
var lessonLocation = doLMSGetValue("cmi.core.lesson_location");
var completo = doLMSGetValue('cmi.core.lesson_status');
var scoreRaw = doLMSGetValue("cmi.core.score.raw");
var retornou = false;
var tempData = [];
var totalAtividades = 5;
var atividadesRespondidas = [0, 0, 0, 0, 0];
var paginasIniciais = ["_mapa-navegacao.html", "_acervo_digital.html", "_creditos.html"]
var startTimeStamp = null;
var progressLocation = [[[0], [0], [0], [0], [0], [0], [0]], [[0], [0], [0], [0], [0], [0], [0]], atividadesRespondidas];
var apAtivos = [];
var audioPlayerAtual = "";
var startDate = 0;
var maiorSlide = 1;
var tentativas = 0;
var scrollPosition = null
var _qdadeBTN = 0;


window.mobileAndTabletCheck = function () {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

var isMobile = mobileAndTabletCheck();

var paginaAtividade = 34; //TODO AJUSTAR PARA RAGINA DO PROJETO
startTimer();


var debug = true; //TODO => MODIFICAR PARA false para sair do DEBUG

var versao = 'v1.0.0 - SENAI/SC 01-09-21';
console.log('versão: ' + versao);


if (suspendData != '') {

    tempData = JSON.parse(suspendData);
    progressLocation[0] = tempData[0]; // paginação dos módulos
    progressLocation[1] = tempData[1];
    totalProgressLocation = tempData[1]; // porcentagem completo de cada modulo
    _ultimoVisitado = lessonLocation.split(';');
    ultimaAulaVisitada = _ultimoVisitado[0];
    ultimoIdVisitado = _ultimoVisitado[1];
    startTimeStamp = sessionStorage.getItem('startTimeStamp');

    //atv final
    if (tempData[2]) {
        progressLocation[2] = tempData[2];
        tentativas = tempData[2][0];
        respostas = Array(tempData[2][1]);
        respostasAcertadas = Array(tempData[2][2]);
    }

    // for (let j = 1; j < ultimoIdVisitado; j++) {
    //     atualizarEstiloMenu(j);
    // }


    if (!sessionStorage.getItem('retornou')) {
        setTimeout(
            function () {
                irparaUltimoVisitado(ultimaAulaVisitada);
            }, 3000);
        sessionStorage.setItem('retornou', true);
        //console.log("retornou");
    }

} else if (sessionStorage.getItem('progressLocation')) {

    tempData = JSON.parse(sessionStorage.getItem('progressLocation'));
    progressLocation[0] = tempData[0]; // paginação
    progressLocation[1] = tempData[1]; // porcentagem
    totalProgressLocation = tempData[1]; // porcentagem de cada modulo
    lessonLocation = sessionStorage.getItem('lessonLocation'); // localização (modulo;session)
    _ultimoVisitado = lessonLocation.split(';');
    ultimaAulaVisitada = _ultimoVisitado[0]; //modulo
    ultimoIdVisitado = _ultimoVisitado[1]; //session
    startTimeStamp = sessionStorage.getItem('startTimeStamp');
    //atv final
    if (tempData[2]) {
        progressLocation[2] = tempData[2];
        tentativas = tempData[2][0];
        respostas = Array(tempData[2][1]);
        respostasAcertadas = Array(tempData[2][2]);
    }

//
}


if (lessonLocation == '') {
    lessonLocation = ';';
}

function atualizaMenu() { // DESBLOQUEIA ETAPAS JÁ COMPLETADAS

    _aux = 0;
    for (let i = 0; i < progressLocation[0].length; i++) {
        if (aulaAtual == 'M' && progressLocation[1][i] >= 100) {
            //console.log('liberou: '+ i+1);
            if (i < _qdadeBTN.length) {
                $('#bloqueado' + (_aux + 1)).addClass("escondido"); //Escode Cadeado
                $('#iniciado' + (_aux + 1)).removeClass("escondido"); //Escode Cadeado
                $('.btn-' + (_aux + 1)).removeClass('bloqueado'); //libera botão
                // $('.btn-' + (_aux + 1)).css('filter', 'none'); // tira filtro cinza
                // $('#check_' + (_aux)).css('visibility', 'visible'); // marca como completado
            }

        }
        _aux++;
    }

    //debugger;
    _totalGeral = 0;
    for (let i = 0; i < _qdadeBTN.length; i++) { //TODO ajustar para quntidade de itens no MENU
        _totalGeral += Math.round(progressLocation[1][i] / _qdadeBTN.length);
    }
    $("#status-progresso").css("width", _totalGeral + "%");

}


function atualizarUltimoVisitado(id) {
    // _ultimoVisitado = lessonLocation.split(';');
    // ultimaAulaVisitada = _ultimoVisitado[0];
    // ultimoIdVisitado = _ultimoVisitado[1];

    // if (ultimaAulaVisitada <= aulaAtual){
    //     if (ultimoIdVisitado < id){
    lessonLocation = aulaAtual + ';' + id;
    sessionStorage.setItem('lessonLocation', lessonLocation);
    doLMSSetLessonLocation(aulaAtual + ";" + id);
    doLMSCommit();

    //console.log('atualizaou location:' + aulaAtual + ' / id:'+id);
    //     }
    //
    //
    // }
//

}

// FUNÇÃO CARREGAR PÁGINAS E ATUALIZAR
function go(id) {
    //debugger;
    //liberarNavegacao = validarNavegacao(suspendData, id);

    // if(navegacaoLinear == 0 || liberarNavegacao == true){
    //
    // 	if(paginaBloqueadaAvanco !=0 && id > paginaBloqueadaAvanco && pontosEncontrados360 < totalPontos360){
    // 		$("#aviso360").show();
    // 	}else{

    //$('head .vjs-styles-dimensions').remove();
    idAtual = id;

    // computeTime();
    //carregarPagina(id);
    if (aulaAtual != 'M') {
        if (Math.max.apply(null, progressLocation[0][aulaAtual]) < id) {

            ultimaAulaVisitada = aulaAtual;
            ultimoIdVisitado = id;
            atualizarUltimoVisitado(ultimoIdVisitado);

        }
        atualizaSuspendData(id);
    }

    //atualizarProgresso();

    atualizarEstiloMenu(id);

    atulizarSetasNavegacao(id);
    atualizaContador(Number(id));

    //closeNav();
    // 	}
    //
    //
    //
    // }else{
    // 	bloquearTeclado=1;
    // 	$("#navegacaoLinear").show();
    // }

}

function irPara(id) {

    //if (id <= Number(ultimoIdVisitado)+1) {
    $.scrollify.move("#" + id);
    go(id);
    //}
}

//FUNÇÃO CARREGAR PÁGINA INICIAL
function carregarPagina(id) {

    //RETORNAR PÁGINA AO TOPO
    // $("#conteudo").scrollTop(0);
    //
    // $.ajax({
    // 	url : 'paginas/pagina-' + id + '.html',
    // 	success : function(data){
    // 		$('#conteudo').html(data);
    // 		//SLIDESHOW INICIAR EM 1
    // 		slideIndex = 1;
    // 	},
    // 	error: function () {
    // 		$('#conteudo').html('<div style="text-align: center"><i class="fa fa-exclamation-triangle fa-6" aria-hidden="true" style="font-size:50px"></i><p>P&#225;gina n&#227;o encontrada.</p></div>');
    // 	}
    // });

    atualizaContador(id);

}

function atualizaContador(id) {

    switch (typeof id) {

        case "string":
            $(".contador").html(id.toUpperCase());
            break;

        default:
            $(".contador").html(id + '/' + limite);
            break;
    }

}


// PÁGINAS
function atualizaSuspendData(id) {
    // var novo = suspendData.split(';');
    // paginas = novo[0];
    // pontos = novo[1];

    //debugger;
    var paginas = progressLocation[0][aulaAtual];

    if (!verificaIdJaVisitado(paginas, id)) {


        progressLocation[0][aulaAtual].push(id);
        // suspendData = JSON.stringify(progressLocation);
        // // sessionStorage.setItem('progressLocation', suspendData );
        // doLMSSetValue("cmi.suspend_data", suspendData);
        // doLMSCommit();
    }
    atualizarProgresso();
    //alert(suspendData);

}

//compara se o id novo ja assiste no array que guarda os ultimos ids acessados
function verificaIdJaVisitado(data, valorId) {
    // idVisitados = data.split(',');
    idVisitados = data;
    for (i = 0; i < idVisitados.length; i++) {
        if (idVisitados[i] == valorId) {
            return true;
        }
    }
    return false;
}


// PONTOS 360
function atualizaSuspendDataPontos(id) {
    var novo = suspendData.split(';');
    paginas = novo[0];
    pontos = novo[1];
    if (!verificaPontosAcessados(pontos, id)) {
        novo = paginas + ';' + pontos + id + ',';
        suspendData = novo;
        doLMSSetValue("cmi.suspend_data", novo);
        doLMSCommit();
        pontosEncontrados360++;
    }

    atualizarProgresso();

}

//compara se o ponto novo ja assiste no array que guarda os ultimos ids acessados
function verificaPontosAcessados(data, valorId) {
    idVisitados = data.split(',');
    for (i = 0; i < idVisitados.length; i++) {
        if (idVisitados[i] == valorId) {
            return true;
        }
    }
    return false;
}

// AVANÇAR PÁGINA
function avancarPagina() {

    if (idAtual != limite) {
        go(idAtual + 1);
    }

}

// VOLTAR PÁGINA
function voltarPagina() {

    if (idAtual != 1) {
        go(idAtual - 1);
    }

}

// VOLTAR E AVANÇAR - SETAS TECLADO
function applyKey(event) {
    if (bloquearTeclado == 0) {
        var value = event.keyCode;
        if (value == 39 || value == 40) {
            avancarPagina();
        } else if (value == 37 || value == 38) {
            voltarPagina();
        }
    }

}

// ATUALIZAR CSS SETAS DE NAVEGAÇÃO
function atulizarSetasNavegacao(id) {

    if (id == 1) {
        $(".navegacao-seta-esquerda").css("opacity", "0.5");
    } else {
        $(".navegacao-seta-esquerda").css("opacity", "1");
    }

    if (id == limite) {
        $(".navegacao-seta-direita").css("opacity", "0.5");
    } else {
        $(".navegacao-seta-direita").css("opacity", "1");
    }

}

// VALIDA AVANÇO NAVEGAÇÃO LINEAR
function validarNavegacao(data, valorId) {

    if (valorId == 1 || data.indexOf(valorId - 1) != -1) {
        return true;
    } else {
        return false;
    }

}

//FUNÇÕES DO MENU CONFIGURAÇÕES

function configuracoes(id) {

    switch (id) {

        case "contraste":
            aplicarContraste();
            break;

        case "fonte":
            aplicarFonte();
            break;

        case "redimensionar":
            aplicarZoom();
            break;

        case "imprimir":
            imprimir();
            break;

        case "ajuda":

            atualizaContador(id);

            $.ajax({
                url: 'paginas/' + id + '.html',
                success: function (data) {
                    $('#conteudo').html(data);
                },
                error: function () {
                    $('#conteudo').html('<div style="text-align: center"><i class="fa fa-exclamation-triangle fa-6" aria-hidden="true" style="font-size:50px"></i><p>P&#225;gina n&#227;o encontrada.</p></div>');
                }
            });

            break;


    }

}

//ZOOM - REDIMENSIONAMENTO
$(window).resize(function () {

    if ($(window).width() <= 600) {

        zoom = 0;
        $("body").css({"-moz-transform": "scale(1.0)", "margin": "0px"});
        $("body").css({"zoom": "1.0"});
        $("body").css("zoom", "1.0");

    }

});

//ZOOM
function aplicarZoom() {

    if (zoom == 0) {
        zoom = 1;

        if ($(window).width() > 600) {

            if (firefox == true) {
                $("body").css({"-moz-transform": "scale(1.1)", "margin": "70px"});
            } else if (ie == true) {
                $("body").css({"zoom": "1.1"});
            } else {
                $("body").css("zoom", "1.1");
            }
        }

    } else if (zoom == 1) {

        zoom = 2;

        if ($(window).width() > 600) {

            if (firefox == true) {
                $("body").css({"-moz-transform": "scale(1.2)", "margin": "130px"});
            } else if (ie == true) {
                $("body").css({"zoom": "1.2"});
            } else {
                $("body").css("zoom", "1.2");
            }
        }

    } else {

        zoom = 0;

        if ($(window).width() > 600) {

            if (firefox == true) {
                $("body").css({"-moz-transform": "scale(1.0)", "margin": "0px"});
            } else if (ie == true) {
                $("body").css({"zoom": "1.0"});
            } else {
                $("body").css("zoom", "1.0");
            }
        }

    }

}

function aplicarContraste() {

    if (contraste == 0) {
        contraste = 1;
        $("#contraste").attr("href", "");
        $("#contraste").attr("href", "./dist/css/modulo0_contraste.css");
    } else {
        contraste = 0;

        $("#contraste").attr("href", "");
    }

}

function aplicarFonte() {

    //
    // if (fonte == 0) {
    //     fonte = 1;
    //     $("#fonte").attr("href", "");
    //     $("#fonte").attr("href", "./dist/css/fonte1.css");
    // } else {
    //     fonte = 0;
    //     $("#fonte").attr("href", "");
    // }

    if (fonte == 0) {
        fonte = 1;
        $("#fonte").attr("href", "");
        $("#fonte").attr("href", "./dist/css/fonte1.css");
    } else if (fonte == 1) {
        fonte = 2;
        $("#fonte").attr("href", "");
        $("#fonte").attr("href", "./dist/css/fonte2.css");
    } else {
        fonte = 0;
        $("#fonte").attr("href", "");
    }

}

// ABRIR JANELA DE IMPRESSÃO NO NAVEGADOR
function imprimir() {
    debugger;
    var conteudo = document.getElementById('conteudoModulo').innerHTML,
        tela_impressao = window.open('about:blank');
    tela_impressao.document.write(conteudo);
    tela_impressao.document.write(conteudo.innerHTML);
    tela_impressao.location.reload();
    tela_impressao.window.print();
    tela_impressao.window.close();

}

// FUNÇÕES ABRIR E FECHAR MENU DE PÁGINAS
function openNav() {
    //debugger;
    if (document.getElementById("NavPaginas").style.width == "36%") {

        closeNav();

    } else {
        var el = 'page-' + ($.scrollify.currentIndex() + 1);
        document.getElementById("NavPaginas").style.width = "36%";
        document.getElementById("NavPaginas").style.display = "block";

        $('.' + el).removeClass("filterOFF").addClass("filterON");

        //document.getElementsByClassName(el)[0].style.filter = 'blur(3px)';

        document.getElementsByClassName('closebtn')[0].style.visibility = 'visible';
    }
    //document.getElementsByClassName('closebtn')[0].style.visibility = 'visible';

}

function closeNav() {
    var el = 'page-' + ($.scrollify.currentIndex() + 1);
    document.getElementById("NavPaginas").style.display = "none";
    document.getElementById("NavPaginas").style.width = "0";
    $('.' + el).addClass("filterOFF").removeClass("filterON");
    // document.getElementsByClassName(el)[0].style.filter = 'none';
    // document.getElementsByClassName('page').style.filter = 'none';
    document.getElementsByClassName('closebtn')[0].style.visibility = 'hidden';

}

function atualizarProgresso() {
    //---PÁGINAS
    //debugger;
    if (aulaAtual != 'M') {
        progressoPaginas = Math.max.apply(null, progressLocation[0][aulaAtual]);

        //console.log('aula Atual: ' + aulaAtual);

        //CONTAGEM PROGRESSO
        totalProgresso = ((progressoPaginas) / (limite));
        valorProgresso = totalProgresso * 100;

        // progressLocation[aulaAtual] = valorProgresso;
        progressLocation[1][aulaAtual] = Math.round(valorProgresso);
        // progressLocation[2][0] = Number(tentativas);
        // progressLocation[2][1] = Array(respostas);
        // progressLocation[2][2] = Array(respostasAcertadas);


        sessionStorage.setItem('progressLocation', JSON.stringify(progressLocation));

        //console.log('TO MOD: ' + progressLocation[1][aulaAtual]);
        $("#status-progresso").css("width", valorProgresso + "%");

    }
    //STATUS "COMPLETED" SCORM
    var status = doLMSGetValue("cmi.core.lesson_status");
    var totalGeral = 0;


    totalGeral += Math.round(progressLocation[1][0] / 1);

    //console.log('TO GE: ' + totalGeral);


    if (status != "completed" && totalGeral == 100) {
        doLMSSetValue("cmi.core.lesson_status", "completed");

    } else {
        doLMSSetValue("cmi.core.lesson_status", "incomplete");

    }
    suspendData = JSON.stringify(progressLocation);
    // sessionStorage.setItem('progressLocation', suspendData );
    doLMSSetValue("cmi.suspend_data", suspendData);
    doLMSCommit();

}


// IR PARA A ÚLTIMA PÁGINA ACESSADA
function irUltimoAcesso() {
    idVisitados = suspendData.split(';');
    // pontosImagem360 = suspendData.split(';');
    idVisitados = idVisitados[0].split(',');
    idUltimoAcesso = parseInt(idVisitados[idVisitados.length - 2]);
    atualizarProgresso();

    // pontosImagem360 = suspendData.split(';');
    // pontosImagem360 = pontosImagem360[1].split(',').length-1;
    // pontosEncontrados360 = pontosImagem360;

    go(idUltimoAcesso);
    fecharAvisos();
}

// IR PARA A ÚLTIMA PÁGINA ACESSADA
function iniciarAcesso() {
    pontosImagem360 = suspendData.split(';');
    pontosImagem360 = pontosImagem360[1].split(',').length - 1;
    pontosEncontrados360 = pontosImagem360;
    fecharAvisos();
}

function fecharAvisos() {
    $("#ultimoAcesso").hide();
    $('#navegacaoLinear').hide();
    $('#aviso360').hide();
    $('#modal').hide();
    bloquearTeclado = 0;
}

// TOOGLEBOX

function toogleBox(id) {

    if ($("#cont-" + id).is(":hidden")) {
        $("#cont-" + id).slideDown("slow");
    } else {
        $("#cont-" + id).slideUp("slow", function () {
            $("#cont-" + id).css("display", "");
        });
    }

}

// REVEALCONT

function revealCont(id) {

    if ($("#contReveal-" + id).is(":hidden")) {
        $("#contReveal-" + id).slideDown("slow");
        $("#revealCont-" + id).css("display", "none");
    }

}

function closecontReveal(id) {
    $("#contReveal-" + id).slideUp("slow", function () {
        $("#contReveal-" + id).css("display", "");
        $("#revealCont-" + id).css("display", "");
    });
}


// SLIDESHOW

var slideIndex = 1;

//showSlides(slideIndex);

function plusSlides(n) {
    //showSlides(slideIndex += n);
}

function currentSlide(n) {
    //showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("myslideshow");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active-slide", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active-slide";
}


// FullOverlay

function on() {
    document.getElementById("fulloverlay").style.display = "block";
}

function off() {
    document.getElementById("fulloverlay").style.display = "none";
}

// Overlay

function overlayn() {
    document.getElementById("fulloverlay").style.display = "block";
}

function overlay(id) {

    if (typeof (id) === 'number') {
        if (id < 2) {
            $("#overlay-" + id).css('display', 'block');
            $("#overlay-" + id).load(paginasIniciais[id]);
        } else {
            $("#overlay-" + id).css('display', 'block');
        }
    } else {
        abreMidiaFancybox('pdf', 'creditos.pdf', 80, 40)
    }
    $.scrollify.disable();
    $("body").css({'overflow-y': 'hidden'});


}

function closeoverlay(id) {
    $("#overlay-" + id).css('display', 'none');
    $.scrollify.enable();
    $("body").css({'overflow-y': 'visible'});
}

//  ZOOM NA IMAGE/LUPA

function magnify(imgID, zoom) {
    //EXECUTAR FUNÇÃO APÓS ALGUN TEMPO
    var variavel = setTimeout(function () {

        var img, glass, w, h, bw;
        img = document.getElementById(imgID);
        /*create magnifier glass:*/
        glass = document.createElement("DIV");
        glass.setAttribute("class", "imgs-magnifier-glass");
        /*insert magnifier glass:*/
        img.parentElement.insertBefore(glass, img);
        /*set background properties for the magnifier glass:*/
        glass.style.backgroundImage = "url('" + img.src + "')";
        glass.style.backgroundRepeat = "no-repeat";
        glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
        bw = 3;
        w = glass.offsetWidth / 2;
        h = glass.offsetHeight / 2;
        /*execute a function when someone moves the magnifier glass over the image:*/
        glass.addEventListener("mousemove", moveMagnifier);
        img.addEventListener("mousemove", moveMagnifier);
        /*and also for touch screens:*/
        glass.addEventListener("touchmove", moveMagnifier);
        img.addEventListener("touchmove", moveMagnifier);

        function moveMagnifier(e) {
            var pos, x, y;
            /*prevent any other actions that may occur when moving over the image*/
            e.preventDefault();
            /*get the cursor's x and y positions:*/
            pos = getCursorPos(e);
            x = pos.x;
            y = pos.y;
            /*prevent the magnifier glass from being positioned outside the image:*/
            if (x > img.width - (w / zoom)) {
                x = img.width - (w / zoom);
            }
            if (x < w / zoom) {
                x = w / zoom;
            }
            if (y > img.height - (h / zoom)) {
                y = img.height - (h / zoom);
            }
            if (y < h / zoom) {
                y = h / zoom;
            }
            /*set the position of the magnifier glass:*/
            glass.style.left = (x - w) + "px";
            glass.style.top = (y - h) + "px";
            /*display what the magnifier glass "sees":*/
            glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
        }

        function getCursorPos(e) {
            var a, x = 0, y = 0;
            e = e || window.event;
            /*get the x and y positions of the image:*/
            a = img.getBoundingClientRect();
            /*calculate the cursor's x and y coordinates, relative to the image:*/
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            /*consider any page scrolling:*/
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;
            return {x: x, y: y};
        }

    }, 100); // TEMPO
}

// POPUP EXTERNO

function popup(url, params) {
    if (typeof params == 'undefined') params = {};
    if (typeof params['win_name'] == 'undefined') params['win_name'] = 'jan_pop';
    if (typeof params['w'] == 'undefined') params['w'] = 810;
    if (typeof params['h'] == 'undefined') params['h'] = screen.height - 55;
    if (typeof params['scroll'] == 'undefined') params['scroll'] = 'yes';
    if (typeof params['resizable'] == 'undefined') params['resizable'] = 'yes';
    params['win'] = window.open(url, params['win_name'], 'scrollbars=' + params['scroll']
        + ',resizable=' + params['resizable'] + ',toolbar=no,location=no,directories=no,'
        + 'menubar=no,status=yes,top=0,left='
        + ((screen.width - params['w']) / 2) + ',width=' + params['w'] + ',height=' + params['h']);
    params['win'].focus();
}

function carregarMenu(_aula) {

    var _aulaAtual = _aula;

    // CARREGAR MENU DE PÁGINAS
    $.ajax({
        type: "GET",
        url: "menus/menu_aula" + _aula + ".xml",
        dataType: "xml",
        success: function (xml) {
            //debugger;

            var idPaginas = 1;
            var totalPaginas = 0;

            // DISCIPLINA
            $(xml).find("Disciplina").each(function () {
                $("#disciplina").html($(this).attr("text"));
            });

            $(xml).find("Titulo").each(function () {
                $(".titulo").html($(this).attr("text"));
            });
            // $(xml).find("page").each(function () {
            //     $(".pagina").html($(this).attr("page"));
            // });


            var ul_main = $("<ul />");


            // PÁGINAS
            var aux = 0;

            $(xml).find("Menu").each(function () {


                // var urlAux = 'modulo' + aux + ".html";
                var urlAux = 'go(' + aux + ')';

                subItens = $(this).children().length;

                var ulSub = $("<ul class='submenu' style='padding-left: 20px; list-style: none;'/>");

                $(this).children().each(function () {

                    // 1º SUBNÍVEL

                    ulSub.append("<li><a data-pagina=" + $(this).attr("page") + " href='javascript: //irPara(" + $(this).attr("page") + ")'><i id=icoVisitado" + $(this).attr("page") + " class='far fa-circle' style='padding-right:10px'></i>" + $(this).attr("text") + "</a></li>");
                    idPaginas++;
                    totalPaginas++;
                    //console.log('TP: ' + totalPaginas);

                });
                //debugger;
                // if (_aulaAtual == aux) {
                //     url = "#"
                // } else {
                //     if (totalProgressLocation[aux] >= 100) {
                url = $(this).attr("page");
                // }
                // }
                // var li = $("<li><a href="+ url +" class='menu'><i style='padding-right:5px;' class='fa fa-plus'></i>" + $(this).attr("text") + "</a></li>");
                var li = $("<li><a class='menu'><i style='padding-right:5px;' class='fa fa-plus'></i>" + $(this).attr("text") + "</a></li>");
                ul_main.append(li.append(ulSub))
                aux++;

            });

            // debugger;
            $("#menu-paginas").append(ul_main);

            $("ul.submenu").hide();

            $("a.menu").click(function () {

                if ($(this).parent().find("ul.submenu").css("display") != "block") {
                    $("ul.submenu").slideUp('fast');
                    $(this).parent().find("ul.submenu").slideDown('fast');


                    $("#menu-paginas").find(".fa-minus").addClass("fa-plus").removeClass("fa-minus");
                    $(this).find(".fa-plus").addClass("fa-minus").removeClass("fa-plus");


                } else {

                    $(this).find(".fa-minus").addClass("fa-plus").removeClass("fa-minus");
                    $("ul.submenu").hide();

                }
            });


            // TOTAL DE PÁGINAS
            // limite = totalPaginas-1;
            // limite = ($('.page').length);

            // CARREGAR PRIMEIRA PÁGINA DO CURSO
            go(idAtual);

            // ATUALIZAR ÍCONES DAS PÁGINAS ACESSADAS
            atualizarIconesPaginasVisitadas();

        }

    });


    // CARREGAR MENU DE CONFIGURAÇÕES
    // $.ajax({
    //     type: "GET",
    //     url: "menus/menu_configuracoes.xml",
    //     dataType: "xml",
    //     success: function (xml) {
    //         var ul_main2 = $("<ul id='menu-config' />");
    //
    //         // PÁGINAS
    //         $(xml).find("Menu").each(function () {
    //
    //             $(this).children().each(function () {
    //
    //                 ul_main2.append("<li class=" + $(this).attr("class") + "><a href='#' onClick=configuracoes('" + $(this).attr("function") + "')>" + $(this).attr("text") + "<i class='" + $(this).attr("icone") + "'></i></a></li>");
    //
    //             });
    //
    //         });
    //
    //         ul_main2.append("<a href='#' class='access_aid close-menu'><i class='fa fa-times'></i></a>");
    //
    //         $("#menu-configuracoes").append(ul_main2);
    //
    //     }
    //
    // });
}

// ATUALIZAR ÍCONES DAS PÁGINAS ACESSADAS
function atualizarIconesPaginasVisitadas() {
    idVisitados = ultimoIdVisitado

    for (i = 0; i <= idVisitados; i++) {
        if ($('[data-pagina=' + i + ']')) {
            $("#icoVisitado" + i).removeClass("fa-circle").addClass("fa-check-circle");
            $('[data-pagina=' + i + ']').addClass("visited-menu");
            $(".pagination a[data-pagina-navigation=" + (i) + "]").addClass("active")
        }

    }
}

function atualizarEstiloMenu(id) {
    //INSERIR ÍCONE NO ITEM DA PÁGINA (MENU-PÁGINAS)
    if ($("#icoVisitado" + id)) {
        $("#icoVisitado" + id).removeClass("fa-circle").addClass("fa-check-circle");
        $('[data-pagina=' + id + ']').addClass("visited-menu");
    }
    verificaPorcentagemModulo(aulaAtual);


    //COLOCAR CLASSE ATIVA NO ITEM DA PÁGINA ATUAL
    atualizarClasseAtivaItemMenu(id);
}

function atualizarClasseAtivaItemMenu(id) {


    $("#menus-paginas ul li ul li a").removeClass("atvmenu");

    if ($("#menus-paginas ul li ul li a").eq(id - 1)) $("#menus-paginas ul li ul li a").eq(id - 1).addClass("atvmenu");

    $("ul.submenu").hide();

    if ($('[data-pagina=' + id + ']')) {
        $('[data-pagina=' + id + ']').parents().eq(2).find("ul.submenu").show();
        $(".menus").removeClass("atvmenu");

        $('[data-pagina=' + id + ']').parents().eq(2).find(".menus").addClass("atvmenu");

        $('[data-pagina=' + id + ']').parents().find(".menus i").removeClass("fa-minus").addClass("fa-plus");

        $('[data-pagina=' + id + ']').parents().eq(2).find(".menus i").removeClass("fa-plus").addClass("fa-minus");

    }
}

function abreImagemFancybox(tipo, caminho, largura, altura) {

    $.fancybox.open({
        padding: 0,
        src: './' + tipo + '/' + caminho,
        type: 'image',
        clickContent: false,
        iframe: {
            css: {
                width: largura,
                height: altura
            }
        }
    });
}

function abreMidiaFancybox(tipo, caminho, largura, altura) {

    $.fancybox.open({
        padding: 0,
        src: './' + tipo + '/' + caminho,
        type: 'iframe',
        iframe: {
            css: {
                width: largura + "vw",
                height: altura + "vw"
            }
        }
    });
}


function abreIlustraFancybox(modulo, caminho, largura, altura) {

    $.fancybox.open({
        padding: 0,
        src: './imgs/modulo' + modulo + '/' + caminho,
        type: 'image',
        clickContent: false,
        iframe: {
            css: {
                width: largura,
                height: altura
            }
        }
    });
}

function irparaUltimoVisitado(id) {
    location.hash = '#1';
    // $('#conteudoModulo').attr('src', 'modulo'+ id +'.html');
    $(function () {
        $("#conteudoModulo").load("modulo" + id + ".html#conteudoModulo");
        aulaAtual = id;
        //go(1);
    });
}

//atualizaMenu();

// $( document ).ready(function() {
//     debugger;
//     if (lessonLocation != ';') {  // primeira fez que acessa o curso
//         if(!sessionStorage.getItem('lessonLocation')){
//             setTimeout(
//                 function () {
//                     irparaUltimoVisitado(ultimaAulaVisitada);
//                 }, 3000);
//         }
//     }
// });


function escondeMao(id) {
    if ($('#mao' + id)) {
        if ($('#mao' + id).hasClass('blinking')) {
            $('#mao' + id).css('visibility', 'hidden');
            $('#mao' + id).removeClass('blinking');
        }
    }
}


var btn = null;
var i;


function ativarBotoes() {
    //debugger;
    mzinha = document.getElementsByClassName("maozinha");

    for (i = 0; i < mzinha.length; i++) {
        //x = mzinha[i].parent.offsetHeight;

        // btnLargura = mzinha[i].offsetWidth;
        // btnAltura = mzinha[i].offsetHeight;
        //
        // posicaoX = btnLargura - btnLargura / 2 + btnLargura / 4;
        // posicaoY =  - btnAltura / 4;
        //
        //
        // mzinha[i].setAttribute('left', posicaoX + 'px');
        // mzinha[i].setAttribute('top', posicaoY + 'px');
        //
        // $('#conteudo').append('<style>.maozinha:after{left:'+posicaoX+'px;}</style>');
        // $('#conteudo').append('<style>.maozinha:after{bottom:'+posicaoY+'px;}</style>');

        mzinha[i].addEventListener("click", function () {
            var panel = this;
            if (panel) {

                panel.classList.remove('maozinha');
            }

            //console.log(panel);
        });
    }


    icones = document.getElementsByClassName("icone-svg");
    for (i = 0; i < icones.length; i++) {

        icones[i].addEventListener("click", function () {
            var panel = this.lastElementChild;
            if (panel.style.visibility === "visible") {
                panel.style.visibility = "hidden";
            }

            // console.log(panel);
        });
    }

    icones = document.getElementsByClassName("btn-svg");
    for (i = 0; i < icones.length; i++) {

        icones[i].addEventListener("click", function () {
            var panel = this.lastElementChild;
            if (panel.style.visibility === "visible") {
                panel.style.visibility = "hidden";
            }

            //console.log(panel);
        });
    }
    //debugger;
    btnSVG = document.getElementsByClassName("botao-svg");
    for (i = 0; i < btnSVG.length; i++) {

        btnSVG[i].addEventListener("click", function () {

            var panel = this.querySelector('.mao');
            if (panel) {
                //panel.classList.add('escondido');
                panel.style.display = "none";
            }

            //console.log(panel);
        });
    }
}

function removerMazinha() {

    btnSVG = document.getElementsByClassName("botao-svg");
    for (i = 0; i < btnSVG.length; i++) {
        if (d3.select(btnSVG[i])) d3.select(btnSVG[i]).select(".mao").remove()
    }
}

var btn = [];
var btnBBox = [];
var mao = [];

function addMaozinhaSVG() {
    btn = [];
    btnBBox = [];


    setTimeout(function () {
        // debugger;
        btn = document.querySelectorAll('.botao-svg');
        if (btn.length === 0) btn = document.querySelectorAll('.btn-svg');
        for (let i = 0; i < btn.length; i++) {
            btnBBox[i] = btn[i].getBBox();
        }

        d3.xml("./dist/imgs/icones/hand-01b.svg")
            .then(data => {
                d3.selectAll(".botao-svg").nodes().forEach(n => {
                    n.append(data.documentElement.cloneNode(true))

                })
                posicionaMao();
            });


    }, 500);

}


function posicionaMao() {
    mao = document.querySelectorAll('.mao');

    for (let i = 0; i < btn.length; i++) {
        btnLargura = btnBBox[i];
        btnAltura = btnBBox[i];
        posicaoX = btnLargura.x + (btnLargura.width - btnLargura.width / 2) - mao[i].getBBox().width / 2;
        posicaoY = btnAltura.y + (btnAltura.height - btnAltura.height / 2) + (btnAltura.height / 4) - mao[i].getBBox().height / 2;
        mao[i].setAttribute('x', posicaoX);
        mao[i].setAttribute('y', posicaoY);
        mao[i].style.display = 'block';
    }


}

function posicionaMao2() {

    btn = document.querySelectorAll('.botao-svg');
    mao = document.querySelectorAll('.mao');
    for (let i = 0; i < btn.length; i++) {
        setTimeout(function () {
            //debugger;
            btnLargura = btn[i].getBBox();
            btnAltura = btn[i].getBBox();

            posicaoX = btnLargura.x + (btnLargura.width - btnLargura.width / 2) - mao[i].getBBox().width / 2;
            posicaoY = btnAltura.y + (btnAltura.height - btnAltura.height / 2) - mao[i].getBBox().height / 2;
            mao[i].setAttribute('x', posicaoX);
            mao[i].setAttribute('y', posicaoY);
            mao[i].setAttribute('display', 'block');
        }, 100);
    }

}


function fecharSVG(id) {
    if (audioPlayerAtual) audioPlayerAtual.pause();

    for (let i = 0; i < btn.length; i++) {

        if ($('#info_' + idAtual + '_' + i)) {
            $('#info_' + idAtual + '_' + i).css('visibility', 'hidden');
        }
    }

}

function abrirSVG(id) {
    for (let i = 0; i < btn.length; i++) {
        if ($('#info_' + idAtual + '_' + i)) {
            $('#info_' + idAtual + '_' + i).css('visibility', 'hidden');
        }
    }
    $('#info_' + idAtual + '_' + id).css('visibility', 'visible');
}

function abrirSVGclick(id) {
    $('#info_' + idAtual + '_' + id).css('visibility', 'visible');
}

// RECURSO SANFONA
function ativarSanfonas() {
    //debugger;
    var acc = document.getElementsByClassName("accordion"); // a varíavel recebe a quantidade de sections que recebem o nome "accordion" que tem na tela.
    var i;
    var aberto;
    for (i = 0; i < acc.length; i++) {   // para cada section executa o for

        acc[i].onclick = function () { // quando clica executa a função
            fecharSanfona();
            if (aberto == this) { // Fecha a section ao clicar nela novamente.
                aberto = null;
                return;
            } else {
                this.classList.toggle("active");
                verificaAberto(this);
            }
            aberto = this; // variavel aberto recebe valor da section anterior
        }

    }

    function fecharSanfona() {//FECHA TODAS AS SANFONAS

        for (i = 0; i < acc.length; i++) {
            _acc = acc[i];
            _acc.classList.remove("active");
            _acc.nextElementSibling.style.maxHeight = null;
            _acc.nextElementSibling.classList.remove("show");

        }
    }

    function verificaAberto(_obj) { // verifica aberto e aplica animação no "panel"

        var panel = _obj.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;

        } else {
            _obj.nextElementSibling.classList.toggle("show");
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    }
}

// FIM RECURSO SANFONA


function atualizarScoreRaw() {
    var scoreLocal = 0;
    for (let i = 0; i < atividadesRespondidas.length; i++) {
        if (atividadesRespondidas[i] == 2) scoreLocal += 20; // verificando notas respondidas
    }


    // let scoreLocalAnterior = doLMSGetValue("cmi.core.score.raw");
    // if (scoreLocal == 70) scoreLocal = 60;
    // scoreLocal = 20 + Number(scoreLocal);

    if (scoreLocal == 60) scoreLocal = 70;
    doLMSSetValue("cmi.core.score.raw", scoreLocal);
    doLMSCommit();
    atualizarProgresso(); // atualiza atividadesRespondidas
}

function adicionaPlayer(ap) {
    apAtivos.push(ap);
}

function resetPlayer(el) {

    let player = document.querySelectorAll(".play");
    for (let i = 0; i < player.length; i++) {
        if (player[i].children[0].classList.contains("fa-spin")) {
            player[i].children[0].classList.remove("fa-spin");
            player[i].children[0].classList.remove("fa-circle-notch");
            player[i].children[0].classList.add("fa-play");
        }
    }
    if (el.children[0].classList.contains("fa-play")) {
        el.children[0].classList.remove("fa-play");
        el.children[0].classList.add("fa-circle-notch");
        el.children[0].classList.add("fa-spin");
    }

}

function liberaBotao(slider) {
    let proximo = $('.' + slider + ' button.slick-next.slick-arrow');
    proximo.removeClass("btn-disabled");
    proximo.addClass("liberado");
}


function abrirModal(id) {
    $.scrollify.disable();
    $('#' + id).removeClass('hidden');

}

function fecharModal(id) {
    $.scrollify.enable();
    $('#' + id).addClass('hidden');
}

/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
var prevScrollpos = window.pageYOffset;

window.onscroll = function () {
    var currentScrollPos = window.pageYOffset;
    var elemento = document.getElementById("navbar");
    if (aulaAtual != 'menu' && elemento) {
        if (prevScrollpos > currentScrollPos) {
            elemento.style.top = "0";
        } else {
            if ($('#menu-principal')) elemento.style.top = "-" + ($('#menu-principal').height() + 'px');
        }
        prevScrollpos = currentScrollPos;
    }

}

// ajustes Moodle e T2K
setTimeout(function () {
    if (top.document.getElementById("scorm_layout")) {
        top.document.getElementById("scorm_layout").style.height = "100vh";
    }

    // Usado no T2K para forçar a saida do LMS e Ajustes de Layout
    if (top.document.getElementById('blsm-qp-exit')) {
        var parent = top.document.getElementById('blsm-qp-exit');
        parent.addEventListener('click', function (event) {
            //unloadPage();
        }, true);
        top.document.getElementById("scorm_content").style.height = "calc(100vh - 30px)";
        top.document.getElementsByClassName('launcher')[0].style.height = "26px";
        top.document.getElementsByClassName('launcher')[0].style.fontSize = "19px";
        top.document.getElementsByClassName('launcher')[0].style.padding = "unset";
        window.addEventListener('resize', function (e) {
            top.document.getElementById("scorm_content").style.height = "calc(100vh - 30px)";
        });
    }

}, 200)


var _videos = '';
var _videosEmbarcados = '';
var _videosOverlay = '';

function setVideos() {
    // _videos = document.querySelectorAll('video');

    _videos = document.getElementsByClassName("player-video-youtube");
    _videosEmbarcados = document.querySelectorAll('video');
    // _videosOverlay = document.getElementsByClassName("player-video-youtube");

}

function setVideosOverlay() {
    // _videos = document.querySelectorAll('video');


}

function pausaVideo() {
    //

    if (_videos != '') {
        for (let j = 0; j < _videos.length; j++) {
            if (_videos[j]) {
                var iframe = _videos[j].getElementsByTagName("iframe")[0].contentWindow;
                iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            }
        }
    }
    if (_videosOverlay != '') {
        for (let j = 0; j < _videosOverlay.length; j++) {
            if (_videosOverlay[j]) {
                var iframe = _videosOverlay[j].getElementsByTagName("iframe")[0].contentWindow;
                iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            }
        }
    }
    if (_videosEmbarcados != '') {
        for (let j = 0; j < _videos.length; j++) {
            if (_videosEmbarcados[j]) _videosEmbarcados[j].pause()
        }
    }

}

function carregaScroll() {

    $(function () {
        $.scrollify({
            section: ".pagina",
            afterRender: function () {
                debugger;
                var pagination = "<ul class=\"pagination\">";
                var activeClass = "";
                $(".pagina").each(function (i) {
                    activeClass = "";
                    if (i === $.scrollify.currentIndex()) {
                        activeClass = "active";
                    }
                    pagination += "<li><a class=\"" + activeClass + "\" href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-name").charAt(0).toUpperCase() + $(this).attr("data-section-name").slice(1) + "</span></a></li>";
                });

                pagination += "</ul>";

                $(".page-1").append(pagination);
                /*

                Tip: The two click events below are the same:

                $(".pagination a").on("click",function() {
                  $.scrollify.move($(this).attr("href"));
                });

                */
                $(".pagination a").on("click", $.scrollify.move);
            }
        });
    });
}

function verificaPorcentagemModulo(modulo) {
    // debugger;
    var _porcentagem = progressLocation[1][modulo];

    if (_porcentagem > 0) {
        $("#iniciado" + modulo).removeClass("escondido");
        $("#bloqueado" + modulo).addClass("escondido");
    }

    if (_porcentagem >= 25) $("#carga" + modulo + "_1").removeClass("escondido");
    if (_porcentagem >= 50) $("#carga" + modulo + "_2").removeClass("escondido");
    if (_porcentagem >= 75) $("#carga" + modulo + "_3").removeClass("escondido");
    if (_porcentagem >= 100) {
        $("#carga" + modulo + "_4").removeClass("escondido");
        $("#iniciado" + modulo).addClass("escondido");
        $("#check" + modulo).removeClass("escondido");
    }

}


