var sliderClass = '.sliderAtvFinal'; // TODO ajustar para o SLIDER correto
var mediaFinal = 0;
var notaMaxima = 100;
var notaMinima = 70;
//var tentativas = 3;
var tipoQuestao = [0,0,0,0,0,0,0,0,0,0];     //tipo:0 radio /tipo:1Combobox / tipo2:V/F / tipo3:checkbox
// var questaoRespondidas = [0,0,0,0,0,0,0,0,0,0]; // 0 = não resondida / 1 = respondida
var questaoRespondidas = 0; //
var respostasAcertadas = [0,0,0,0,0,0,0,0,0,0]; // 0 = errada / 1 = correta
var respostasAcertadas_antiga = [0,0,0,0,0,0,0,0,0,0]; // armazena corretas da tentativa anterior
var gabarito = [0,0,0,0,0,0,0,0,0];
var respostas = [0,0,0,0,0,0,0,0,0];
var respostas_antiga = [0,0,0,0,0,0,0,0,0]; // armazena respostas da tentativa anterior
var msgArmazenado = "<p>Sua resposta foi armazenada!</p>";
var msgPreenchaTodasLacunas = "<p>Pelo menos um campo est&aacute; em branco.</p><p>Verifique antes de enviar sua resposta novamente.</p>";
var msgSelecioneUmaOpcao = "<p>A resposta n&atilde;o pode ser enviada em branco. Assinale uma das alternativas.</p>";


function respostaArmazenada(msgArmazenado,questao_id, tipo, resposta, acertou) {
//debugger;
    if (idAtual === paginaAtividade && paginaId === 1) {  //  Reinicia tentativa

        questaoRespondidas = 0;
        respostasAcertadas = [];
        respostas = [];
    }


    tipoQuestao[questao_id] = tipo;
    questaoRespondidas++;
    respostas[questao_id] = resposta;
    respostasAcertadas[questao_id] = acertou;

    //let aux = 0;
    // for (let i = 0; i < questaoRespondidas.length; i++) {
    //     if (questaoRespondidas[i] === 1 ) aux++ //quantifica questões respondidas
    // }

    liberaBotao('sliderAtvFinal');

    if (questaoRespondidas === 10 ) {
        apresentaResultados(); //TODO
    }
    else {
        new Messi(msgArmazenado, {title: 'Atenção!', modal: true}); //TODO
    }
}


function repostaSelecioneUmaOpcao() {
    new Messi(msgSelecioneUmaOpcao, {title: 'Aten\u00e7\u00e3o!', modal: true});
}

function repostaMarqueTodosAlternativas(){
    new Messi(msgPreenchaTodasLacunas, {title: 'Atenção!', modal: true});
}

function repostaSelecionouAlternativa() {
    new Messi('Selecione uma alternativa para cada pergunta para finalizar está atividade.', {title: 'Aten\u00e7\u00e3o!', modal: true});
}

function apresentaResultados() {
    //debugger;
    if (tentativas <=3 ){
        tentativas ++;

        let aux = 0;
        for (let i = 0; i < respostasAcertadas.length; i++) {
            if (Number(respostasAcertadas[i]) === 1 ) aux++ //quantifica questões respondidas corretamente
        }

        mediaFinal = (aux*10);

        if (scoreRaw == '') scoreRaw = 0;

        if (mediaFinal >= scoreRaw) {
            scoreRaw = mediaFinal;
            doLMSSetValue("cmi.core.score.raw", scoreRaw);
        }

        // atualiza o json

        // json.scoreRaw = Number(scoreRaw);
        progressLocation[2][0] = Number(tentativas);


        if (mediaFinal >=70){
            //    aprovado

            progressLocation[2][1] = Array(respostas);
            progressLocation[2][2] = Array(respostasAcertadas);
            abreAprovado();


        }else{
            //    reprovado
            if (tentativas === 0) { // restaura resultado tantativa anterior
                progressLocation[2][1] = Array(respostas);
                progressLocation[2][2] = Array(respostasAcertadas);
            }

            abreReprovado();

        }
        atualizarProgresso();


    }else{
        abreTentativasBloqueada(bloqueado);
    }
    $('.sliderAtvFinal').slick('slickNext');


}

function _mediaFinal(){
    return mediaFinal;
}

function _tentativas(){
    return 3 - tentativas;
}

// var msgParabens = '<p>Parabéns! Você atingiu a nota '+ _mediaFinal() +' e foi aprovado!<br>' +
//     'Você aprendeu conhecimentos importantes sobre Língua Brasileira de Sinais (Libras) que com certeza vão lhe ajudar em seu percurso profissional e na vida social com as pessoas surdas.\n</p>';
// var msgError = '<p>Atenção! Você atingiu a nota '+ _mediaFinal() +' que não é suficiente para aprovação no curso. <br> \n ' +
//     'Você tem agora apenas '+ _tentativas() +' tentativas!\n</p>';
var bloqueado = '<p>Infelizmente acabaram suas tentativas e você não alcançou a nota suficiente para aprovação no curso. \n</p>';

function abreAprovado() {

    new Messi('<p>Parabéns! Você atingiu a nota '+ _mediaFinal() +' e foi aprovado!<br>' +
        'Você aprendeu conhecimentos importantes sobre  introdução à tecnologia da informação e comunicação que com certeza vão lhe proporcionar o desenvolvimento de capacidades básicas relativas à comunicação e ao uso de ferramentas de TIC nos processos de comunicação no trabalho.\n</p>', {title: 'Parabéns!', modal: true});
}

function abreReprovado() {
    new Messi('<p>Atenção! Você atingiu a nota '+ _mediaFinal() +' que não é suficiente para aprovação no curso. <br> \n ' +
        'Você tem agora apenas '+ _tentativas() +' tentativas!<br><br>\n' +
        'Para refazer o Desafio Final você deve fechar essa caixa, e na sequência voltar a plataforma e entrar novamente na Unidade Curricular. <br> \n' +
        'Por fim, acesse a tela do Desafio Final e refaça as atividades.\n</p>', {title: 'Fique atento!', modal: true});
}


function abreTentativasBloqueada(bloqueado){
    new Messi(bloqueado, {title: 'Atenção!', modal: true});

}

function verificaTentativas(){
    //debugger;
    if (tentativas >= 3 && paginaId === 0) {
        bloqueiaAtividades();
    }else{
        if (tentativas != 0 && paginaId === 0) novaTentativa();
    }


}

function textoTentativas(){

    return _tentativas() > 1 ? ' tentativas!' : ' tentativa!';
}

function novaTentativa() {
    new Messi('<p>Você tem agora apenas '+ _tentativas() +' '+ textoTentativas() +'<br> Caso queira iniciar uma nova tentativa, responda a questão a seguir e clique em "ENVIAR". </p>', {title: 'Fique atento!', modal: true});
}

function bloqueiaAtividades(){
    abreTentativasBloqueada(bloqueado);
    $('.sliderAtvFinal').slick('slickGoTo', 11);
}



// SCRIPTS ATIVIDADES

// ATIVIDADE RADIO
function validaRadioFinal(formulario, formularioID, _correta) {
//debugger;
    var ok = true;
    var correta = _correta; // <==== NUMERO DA RESPOSTA CORRETA
    var _resposta = ''
    var numForm = parseInt(formularioID);

    //Define o número do formulário
    let form = '';
    for (let i = 0; i < document.forms.length; i++) {
        if (document.forms[i].id == numForm){
            form = document.forms[i];
            // numForm = form.id;
        }
    }


    //Verifica se alguma alternativa foi selecionada
    if (!tudoChecado(formulario)) {
        repostaSelecioneUmaOpcao();
        return;
    } else {
        form.querySelector('#enviar').disabled = true;

        //Verifica qual alternativa foi selecionada
        for (var i = 0; i < form.length; i++) {
            var controle = form.elements[i];
            var objeto = document.getElementById("label" + i);

            if (i !== [ form.length - 1 ]) {

                if (controle.checked) {
                    _resposta = Number(controle.value);
                    if (controle.value != correta) {

                        ok = false;
                    }
                } else {

                }
            }
        }

        //Define número do label
        // var numLabel = selecionada - 1;

        $('#enviar').hide(); //escode enviar

        //verifica se a resposta está correta
        if (ok) {

            respostaArmazenada(msgArmazenado,numForm,0, _resposta, 1);

        } else {
            respostaArmazenada(msgArmazenado,numForm,0, _resposta, 0);


        }
    }
}


// ATIVIDADE V/F
function validaPreencherFinal(formulario, formularioID) {
    var _respostas = [];
    var ok = true;
    if (camposVazios(formulario)) {
        repostaMarqueTodosAlternativas();
        return;
    }

    //Define o número do formulário
    var numForm = parseInt(formularioID);



    // var color = getColorTexto();
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        var objeto = document.getElementById("label" + i);

        if (i != [formulario.length - 1]) {
            _respostas[i] = controle.value.toLowerCase();
            if (controle.className.toLowerCase() != controle.value.toLowerCase()) {

                ok = false;

            }
        }
    }
    form.elements.enviar.style.visibility = 'hidden';  //Faz o botao enviar sumir

    if (ok) {
        respostaArmazenada(msgArmazenado,numForm,2, _respostas, 1);
    } else {
        respostaArmazenada(msgArmazenado,numForm,2, _respostas, 0);
    }
}


// ATIVIDADE COMBOBOX
function validaSelectFinal(formulario, formularioID) {
//debugger;
    var _respostas = [];

    var alternativasCertas = 0;
    let form = '';
    var numForm = '';
    for (let i = 0; i < document.forms.length; i++) {
        if (document.forms[i].id == formularioID) {
            form = document.forms[i];
            numForm = parseInt(form.id);
        }
    }


    if (selectsVazios(formulario)) {
        repostaSelecionouAlternativa();
        return;
    } else {

        var select = form.getElementsByTagName('select');
        var qtd_selects = select.length;
        for (var i = 0; i < qtd_selects; i++) {
            // console.log(select[i].value);
            // console.log(select[i].className);

            _respostas[i] = select[i].value; // Armazena as respostas

            if (select[i].value != select[i].className) {
                var idSelect = select[i].value;
            } else {
                alternativasCertas++;
                // console.log('acertou '+ i+ ':'+select[i].className);
            }
        }

        form.elements.enviar.style.visibility = 'hidden';  //Faz o botao enviar sumir

        if (alternativasCertas == qtd_selects) {
            respostaArmazenada(msgArmazenado,numForm,1, _respostas, 1);

        } else {
            respostaArmazenada(msgArmazenado,numForm,1, _respostas, 0);
        }


    }
}

//Multiplas respostas (checkbox)
function validaMultiplaFinal(formulario,formularioID,nAlternativas) {
    //debugger;
    var _respostas = [];
    var numForm = parseInt(formularioID);
    let form = '';
    for (let i = 0; i < document.forms.length; i++) { // seleciona o FORM correto
        if (document.forms[i].id == formularioID){
            form = document.forms[i];
        }
    }

    form.elements.enviar.style.visibility = 'hidden';  //Faz o botao enviar sumir

    var ok = true;
    var alternativas = 0;
    var color = getColorTexto();
    for ( var i = 0; i < form.length; i++) {
        var controle = form.elements[i];
        var objeto = document.getElementById("label" + i);
        if (i < (form.length - 1)) {
            _respostas[i] = controle.checked; // Armazena as respostas

            if (controle.checked) {

                if (controle.value === "false") {

                    ok = false;
                } else {
                    alternativas++;
                }
            }
        }
    }
    if (ok) {
        respostaArmazenada(msgArmazenado,numForm,3, _respostas, 1);
    } else {
        respostaArmazenada(msgArmazenado,numForm,3, _respostas, 0);
    }
}





function camposVazios(formulario) {
    var vazio = false;
    for ( var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        if (i != [ formulario.length - 1 ]) {
            if (controle.value == "") {
                vazio = true;
            }
        }
    }
    return vazio;
}

function selectsVazios(formulario) {
    var checados = false;
    var check = "selecione";
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        var objeto = document.getElementById("label" + i);
        if (i != [formulario.length - 1]) {
            if (controle.value.toLowerCase() == check.toLowerCase()) {
                checados = true;
            }
        }
    }
    return checados;
}

function tudoChecado(formulario) {
    var checados = false;
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        if (i != [formulario.length - 1]) {
            if (controle.checked) {
                checados = true;
            }
        }
    }
    return checados;
}
