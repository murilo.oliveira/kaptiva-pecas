var _maiorVisitado = 0;
var _cursoCarregado = false;

var _controleTotalPaginas = 0;
$.scrollify({
    section: "section",
    // sectionName: "page",
    // interstitialSection: "",
    // easing: "easeOutExpo",
    // scrollSpeed: 1100,
    // offset: 0,
    // scrollbars: false,
    // standardScrollElements: ".normal",
    // setHeights: true,
    // overflowScroll: true,
    // updateHash: true,
    // touchScroll: true,
    before: function (i, panels) {
        //debugger
        
        var _vetorIds = []
        var _aux = (Number(i) + 1);
        if (_maiorVisitado < Number(i)) _maiorVisitado = Number(i);
        if (_maiorVisitado < _aux) {
            if ((_aux % 5) === 0) {
                _controleTotalPaginas = 0;

                for (let j = 1; j < 6; j++) {

                    var _pagina = Number(_aux + 5 + j);
                    _vetorIds[j] = Number(_pagina);
                    if (_paginas < _pagina) {
                        return;
                    } else {
                        $(".page-" + _pagina).load("p-m"+aulaAtual+"-" + _pagina + ".html", function () {
                            recarregarRecursos(_vetorIds[j]);
                        });

                    }
                }
            }
        }
        $(".pagination").find("a[data-pagina-navigation='" + (i + 1) + "']").addClass("active");

        if ($.fancybox.getInstance()) $.fancybox.getInstance().close();
        pausaVideo();
        if (audioPlayerAtual) audioPlayerAtual.pause();
        if (debug) {
            $("#pagina_html").text('p-m'+aulaAtual+'-' + (i + 1) + '.html');
        } // -->PAGINA ATUAL DEBUG
        closeNav();
        go(i + 1);


    },

    after: function () {

        // console.log('after');
        //console.log($.scrollify.currentIndex());

        // if (paginaAtividade === (Number($.scrollify.currentIndex())) + 1) {
        //     verificaTentativas();
        // }

    },
    afterResize: function () {
    },
    afterRender: function () {

        //debugger;
        // var pagination = "<ul class=\"pagination\">";
        // var activeClass = "";
        // $(".pagina").each(function (i) {
        //     activeClass = "";
        //     if (i === $.scrollify.currentIndex()) {
        //         activeClass = "active";
        //     }
        //     pagination += "<li><a data-pagina-navigation='" + (i + 1) + "' class=\"" + activeClass + "\" onclick=\"irPara(" + (i + 1) + ")\"></a></li>";
        // });
        //
        // pagination += "</ul>";

        //$(".navegador").append(pagination);
        /*

        Tip: The two click events below are the same:

        $(".pagination a").on("click",function() {
          $.scrollify.move($(this).attr("href"));
        });

        */
        //$(".pagination a").on("click", $.scrollify.move);

    }

});
// $.scrollify.disable();


