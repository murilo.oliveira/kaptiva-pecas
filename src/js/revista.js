function carregaRevista(id) {
    //debugger;
    //$('#canvas').fadeIn(1000);

    var _flipbook = $('#'+id);

    // Check if the CSS was already loaded

    if (_flipbook.width() == 0 || _flipbook.height() == 0) {
        setTimeout(carregaRevista(id), 10);
        return;
    }

    // Create the flipbook

    _flipbook.turn({

        // Magazine width

        width: "70vw",

        // Magazine height

        height: "33vw",

        // Duration in millisecond

        duration: 1000,

        // Hardware acceleration

        acceleration: !isChrome(),

        // Enables gradients

        gradients: true,

        // Auto center this flipbook

        autoCenter: true,

        // Elevation from the edge of the flipbook when turning a page

        elevation: 50,

        // The number of pages

        pages: 4,

        // Events

        when: {
            turning: function (event, page, view) {

                var book = $(this),
                    currentPage = book.turn('page'),
                    pages = book.turn('pages');

                // Update the current URI

                // Hash.go('page/' + page).update();

                // Show and hide navigation buttons

                disableControls(page);


                // $('.thumbnails .page-' + currentPage).parent().removeClass('current');
                //
                // $('.thumbnails .page-' + page).parent().addClass('current');


            },

            turned: function (event, page, view) {

                disableControls(page);

                // $(this).turn('center');

                if (page == 1) {
                    $(this).turn('peel', 'br');
                }

            },

            missing: function (event, pages) {

                // Add pages that aren't in the magazine

                for (var i = 0; i < pages.length; i++)
                    addPage(pages[i], $(this));

            }
        }

    });

    // Zoom.js

    $('.magazine-viewport').zoom({
        flipbook: _flipbook,

        max: function () {

            return largeMagazineWidth() / _flipbook.width();

        },

        when: {

            swipeLeft: function () {

                $(this).zoom('flipbook').turn('next');

            },

            swipeRight: function () {

                $(this).zoom('flipbook').turn('previous');

            },

            // resize: function (event, scale, page, pageElement) {
            //
            // 	if (scale == 1)
            // 		loadSmallPage(page, pageElement);
            // 	else
            // 		loadLargePage(page, pageElement);
            //
            // },

            zoomIn: function () {

                $('.thumbnails').hide();
                $('.made').hide();
                $('.magazine').removeClass('animated').addClass('zoom-in');
                $('.zoom-icon').removeClass('zoom-icon-in').addClass('zoom-icon-out');

                if (!window.escTip && !$.isTouch) {
                    escTip = true;

                    $('<div />', {'class': 'exit-message'}).html('<div>Press ESC to exit</div>').appendTo($('body')).delay(2000).animate({opacity: 0}, 500, function () {
                        $(this).remove();
                    });
                }
            },

            zoomOut: function () {

                $('.exit-message').hide();
                $('.thumbnails').fadeIn();
                $('.made').fadeIn();
                $('.zoom-icon').removeClass('zoom-icon-out').addClass('zoom-icon-in');

                setTimeout(function () {
                    $('.magazine').addClass('animated').removeClass('zoom-in');
                    resizeViewport();
                }, 0);

            }
        }
    });

    // Zoom event

    if ($.isTouch)
        $('.magazine-viewport').bind('zoom.doubleTap', zoomTo);
    else
        $('.magazine-viewport').bind('zoom.doubleTap', zoomTo);


    // Using arrow keys to turn the page

    $(document).keydown(function (e) {

        var previous = 37, next = 39, esc = 27;

        switch (e.keyCode) {
            case previous:

                // left arrow
                _flipbook.turn('previous');
                e.preventDefault();

                break;
            case next:
//right arrow
                _flipbook.turn('next');
                e.preventDefault();

                break;
            case esc:

                $('.magazine-viewport').zoom('zoomOut');
                e.preventDefault();

                break;
        }
    });

    // URIs - Format #/page/1

    Hash.on('^page\/([0-9]*)$', {
        yep: function (path, parts) {
            var page = parts[1];

            if (page !== undefined) {
                if (_flipbook.turn('is'))
                    _flipbook.turn('page', page);
            }

        },
        nop: function (path) {

            if (_flipbook.turn('is'))
                _flipbook.turn('page', 1);
        }
    });


    $(window).resize(function () {
        resizeViewport();
    }).bind('orientationchange', function () {
        resizeViewport();
    });

    // Regions

    if ($.isTouch) {
        _flipbook.bind('touchstart', regionClick);
    } else {
        _flipbook.click(regionClick);
    }

    // Events for the next button

    $('.next-button').bind($.mouseEvents.over, function () {

        $(this).addClass('next-button-hover');

    }).bind($.mouseEvents.out, function () {

        $(this).removeClass('next-button-hover');

    }).bind($.mouseEvents.down, function () {

        $(this).addClass('next-button-down');

    }).bind($.mouseEvents.up, function () {

        $(this).removeClass('next-button-down');

    }).click(function () {

        _flipbook.turn('next');

    });

    // Events for the next button

    $('.previous-button').bind($.mouseEvents.over, function () {

        $(this).addClass('previous-button-hover');

    }).bind($.mouseEvents.out, function () {

        $(this).removeClass('previous-button-hover');

    }).bind($.mouseEvents.down, function () {

        $(this).addClass('previous-button-down');

    }).bind($.mouseEvents.up, function () {

        $(this).removeClass('previous-button-down');

    }).click(function () {

        _flipbook.turn('previous');

    });


    resizeViewport();

    _flipbook.addClass('animated');

}

// Zoom icon

$('.zoom-icon').bind('mouseover', function () {

    if ($(this).hasClass('zoom-icon-in'))
        $(this).addClass('zoom-icon-in-hover');

    if ($(this).hasClass('zoom-icon-out'))
        $(this).addClass('zoom-icon-out-hover');

}).bind('mouseout', function () {

    if ($(this).hasClass('zoom-icon-in'))
        $(this).removeClass('zoom-icon-in-hover');

    if ($(this).hasClass('zoom-icon-out'))
        $(this).removeClass('zoom-icon-out-hover');

}).bind('click', function () {

    if ($(this).hasClass('zoom-icon-in'))
        $('.magazine-viewport').zoom('zoomIn');
    else if ($(this).hasClass('zoom-icon-out'))
        $('.magazine-viewport').zoom('zoomOut');

});

$('#canvas').hide();


// Load the HTML4 version if there's not CSS transform

// yepnope({
// 	test: Modernizr.csstransforms,
// 	yep: ['dist/js/libs/turnJS/turn.js'],
// 	nope: ['dist/js/libs/turnJS/turn.html4.min.js'],
// 	both: ['dist/js/libs/turnJS/zoom.js', 'dist/js/libs/turnJS/magazine.js', 'dist/js/libs/turnJS/magazine.css'],
// 	complete: loadApp
// });

