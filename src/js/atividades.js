// Script com validadores das atividades
var controleRespostas = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];

function getColorTexto() {
    var color;
    if ($("#conteudo").attr('class') === 'alto-contraste-desligado') {
        color = "#000";
    } else {
        color = "";
    }
    return color;

}
function controladorRespostas(id){
    if (controleRespostas[id] == 0) controleRespostas[id] = 1;
    console.log(controleRespostas);
    //return controleRespostas[id];
}

function respostaOK(id){
    if (controleRespostas[id] == 0){
        return false;
    }else{
        return true;
    }
}

function liberarBotao(id) {
    if (id != undefined) {
        if (respostaOK(id)) {
            $('button.slick-next.slick-arrow').removeClass("btn-disabled");
        }else{
            $('button.slick-next.slick-arrow').addClass("btn-disabled");
        }
    }
}


function validaPreencherVF(formulario, formularioID) {
    // debugger;
    var ok = true;
    let form = '';
    for (let i = 0; i < document.forms.length; i++) {
        if (document.forms[i].id == formularioID) {
            form = document.forms[i];
        }
    }

    if (camposVazios(form)) {
        repostaMarqueTodosAlternativas();
        return;
    }


    var color = getColorTexto();
    for (var i = 0; i < form.elements.length; i++) {
        var controle = form.elements[i];
        //var objeto = form.elements.namedItem("label" + i);
        if (i != [form.length - 1]) {
            if (controle.className.toLowerCase() != controle.value.toLowerCase()) {
                controle.style.color = "red";
                //$("#label" + i + " i").css('color', 'red');
                ok = false;
            } else {
                controle.style.color = "#000";
                //$("#label" + i + " i").css('color', color);
            }
        }
    }
    form.elements.enviar.style.visibility = 'hidden';

    if (ok) {

        resposta(form.id.substring(1, form.id.length),0);
    } else {

        respostaErrada(form.id.substring(1, form.id.length), 1);

    }
}


function validaAtividadeRadio(formulario, formID, correta) {
    //debugger;
    var ok = true;
    var numForm;
    let form = '';
    for (let i = 0; i < document.forms.length; i++) {
        if (document.forms[i].id == formID) {
            form = document.forms[i];
        }
    }


    if (!tudoChecado(form)) {
        repostaSelecioneUmaOpcao();
        return;
    } else {
        //form.querySelector('#enviar').disabled = true;
        //form.elements.enviar.style.visibility = 'hidden';
    }
    var color = getColorTexto();
    for (var i = 0; i < form.length; i++) {
        var controle = form.elements[i];
        var objeto = document.getElementById("label" + i);

        if (i !== [form.length - 1]) {
            if (controle.checked) {
                numForm = controle.value
                if (controle.value != correta) {
                    ok = false;
                }
            } else {

            }
        }
    }
    // new Messi(msg, {title: 'Parab\u00e9ns!', modal: true});
    if (ok) {
//debugger;
        //document.querySelector('#' + form.id + ' button.btn-success').style.display = 'none';
        resposta(form.id.substring(1, form.id.length), Number(numForm) - 1); // CORRETA
        // liberarBotao(form.id);
    } else {
        console.log()
        respostaErrada(form.id.substring(1, form.id.length), Number(numForm) - 1); // ERRADA
    }
}

//Fun��o que valida os formul�rios que tem select // Antiga fun��o validaBox
function validaSelect(formulario, formularioID, respostaid, metaOuro, posicaoArray) {
    // debugger;
    var alternativasCertas = 0;
    //Define o n�mero do formul�rio
    var numForm = parseInt(formularioID);
    let form = '';
    for (let i = 0; i < document.forms.length; i++) {
        if (document.forms[i].id == formularioID) {
            form = document.forms[i];
        }
    }


    if (selectsVazios(formulario)) {
        repostaSelecionouAlternativaErrada();
        return;
    } else {

        var select = form.getElementsByTagName('select');
        var qtd_selects = select.length;

        //Verifica se as alternativas est�o certas
        for (var i = 0; i < qtd_selects; i++) {
            if (select[i].value != select[i].className) {
                var idSelect = select[i].value;
                //$("#" + idSelect).css('color', colorError);

            } else {
                alternativasCertas++;
            }
        }

        //Faz o bot�o enviar sumir
        // form.elements.enviar.style.visibility = 'hidden';
        $('#enviar' + form.id).hide();

        //Verifica se todas est�o corretas
        if (alternativasCertas == qtd_selects) {
            for (var i = 0; i < qtd_selects; i++) {
                var idSelect = select[i].value;
                //$("#" + idSelect).after(imgCheck);
            }

            //respostaCorreta(msgAcerto);
            resposta(form.id.substring(1, form.id.length), 0); // CORRETA
            // liberarBotao(form.id);
        } else {
            respostaErrada(form.id.substring(1, form.id.length), 1); // ERRADA
        }
    }
}

function selectsVazios(formulario) {
    var checados = false;
    var check = "selecione";
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        var objeto = document.getElementById("label" + i);
        if (i != [formulario.length - 1]) {
            if (controle.value.toLowerCase() == check.toLowerCase()) {
                checados = true;
            }
        }
    }
    return checados;
}


function validaMultipla(formulario, formularioID, nAlternativas, msgFinal, numeroResposta, metaOuro, posicaoArray) {
    // debugger;
    var numForm = parseInt(formularioID);
    let form = '';
    for (let i = 0; i < document.forms.length; i++) { // seleciona o FORM correto
        if (document.forms[i].id == formularioID) {
            form = document.forms[i];
        }
    }

    var ok = true;
    var alternativas = 0;
    var color = getColorTexto();
    for (var i = 0; i < form.length; i++) {
        var controle = form.elements[i];
        var objeto = document.getElementById("label" + i);
        if (i !== [form.length - 1]) {
            if (controle.checked) {
                if (controle.value === "false") {

                    ok = false;
                } else {
                    alternativas++;
                }
            } else {
                //objeto.style.color = color;
                //$("#label" + i + " i").css('color', color);
            }
        }
    }
    if (ok) {
        if (alternativas === nAlternativas) {
            // if (!msgFinal) {
            //     mostrarMsg();
            // }else{
            resposta(form.id.substring(1, form.id.length) ,0);
            // }
        } else {
            respostaErrada(form.id.substring(1, form.id.length),1);
            //repostaMarqueTodasAsAlternativas();
        }
    } else {
        respostaErrada(form.id.substring(1, form.id.length),1);
    }
}

function mostrarMsg() {
    respostaparabensFinal();
}

function tudoChecado(formulario) {
    var checados = false;
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        if (i != [formulario.length - 1]) {
            if (controle.checked) {
                checados = true;
            }
        }
    }
    return checados;
}

function camposVazios(formulario) {
    var vazio = false;
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        if (i != [formulario.length - 1]) {
            if (controle.value == "") {
                vazio = true;
            }
        }
    }
    return vazio;
}

function boxVazias(formulario) {
    var checados = false;
    var check = "selecione";
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        var objeto = document.getElementById("label" + i);
        if (i != [formulario.length - 1]) {
            if (controle.value.toLowerCase() == check.toLowerCase()) {
                checados = true;
            }
        }
    }
    return checados;

}

function verificaCombo(formulario, numero, metaOuro, posicaoArray) {
    var form = formulario.id;
    var corretaF = 0;
    formulario.querySelector('#enviar').disabled = true;
    for (i = 0; i < $("#" + form + " > ol > li").length; i++) {
        if ($("#" + form + " > ol > li:eq(" + i + ") > p > select > option").filter(":selected").attr("id") == $("#" + form + " > ol > li:eq(" + i + ") > p > select").attr("id")) {
            corretaF++;


            $("#" + form + " > ol > li:eq(" + i + ") > p").css({color: "#295669"});
        } else {
            $("#" + form + " > ol > li:eq(" + i + ") > p").css({color: "red"});
        }
    }

    if (corretaF == $("#" + form + " > ol > li").length) {

        var str = form;
        var res = str.replace("form", "");
        //console.log('acertou!');
        resposta(contPaginaP, metaOuro, posicaoArray);
    } else {
        //console.log('Errou!');
        respostaErrada(metaOuro);
        corretaF = 0;
    }

}

// função que valida as astividades verdadeiro e falso
var msgPreenchaTodasLacunas = "<p style='text-align: center'>Pelo menos um campo est&aacute; em branco. <br><br>Verifique antes de enviar sua resposta novamente.</p>";
var msgAcerto = "<p style='text-align: center'>Parabéns, você conseguiu compreender os conceitos básicos de lógica de programação. \n</p>";
var msgErro = "<p style='text-align: center'>Retorne à aula 03 e reveja o conteúdo sobre introdução à lógica de programação.\n</p>";

function validaPreencher(formulario, formularioID, msgFinal) {
    var ok = true;
    if (camposVazios(formulario)) {
        repostaMarqueTodosAlternativas();
        return;
    }

    var color = getColorTexto();
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        var objeto = document.getElementById("label" + i);
        if (i != [formulario.length - 1]) {
            if (controle.className.toLowerCase() != controle.value.toLowerCase()) {
                objeto.style.color = "red";
                $("#label" + i + " i").css('color', 'red');
                ok = false;
            } else {
                objeto.style.color = "#000";
                $("#label" + i + " i").css('color', color);
            }
        }
    }
    if (ok) {
        new Messi(msgAcerto, {title: 'Parab\u00e9ns!', modal: true});
    } else {
        new Messi(msgErro, {title: 'Fique atento!', modal: true});
    }
}

function camposVazios(formulario) {
    var vazio = false;
    for (var i = 0; i < formulario.length; i++) {
        var controle = formulario.elements[i];
        if (i != [formulario.length - 1]) {
            if (controle.value == "") {
                vazio = true;
            }
        }
    }
    return vazio;
}

function repostaMarqueTodosAlternativas() {
    new Messi(msgPreenchaTodasLacunas, {title: 'Atenção!', modal: true});
}

function getColorTexto() {
    var color;
    if ($("#conteudo").attr('class') == 'alto-contraste-desligado') {
        color = "#000";
    } else {
        color = "#FFF";
    }
    return color;

}
