function resposta(num, tipo){

    controladorRespostas(num);
    //liberarBotao(num);
    new Messi(msgNum(num, tipo), {title: 'Parab\u00e9ns!', modal: true});

}

function respostaErrada(num, tipo){
    new Messi(msgNum(num, tipo), {title: 'Ops!', modal: true});
}

function repostaMarqueTodosAlternativas(){
    new Messi(msgPreenchaTodasLacunas, {title: 'Aten\u00e7\u00e3o!', modal: true});
}

function repostaSelecioneUmaOpcao(){
    new Messi(msgSelecioneUmaOpcao, {title: 'Aten\u00e7\u00e3o!', modal: true});
}

function repostaMarqueTodasAsAlternativas(){
    new Messi(msgMarqueTodasAsAlternativas, {title: 'Aten\u00e7\u00e3o!', modal: true});
}

function repostaSelecionouAlternativaErrada(){
    new Messi(msgSselecionouAlternativaErrada, {title: 'Aten\u00e7\u00e3o!', modal: true});
}

function respostaparabensFinal(){
    new Messi(msgParabensFinal, {title: 'Parab\u00e9ns!', modal: true});
}

function getColorTexto() {
    var color;
    if ($("#conteudo").attr('class') === 'alto-contraste-desligado') {
        color = "#000";
    } else {
        color = "";
    }
    return color;

}



