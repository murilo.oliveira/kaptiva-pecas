var cliqueArraste = (function (_numeroAtividade) {
    var numeroAtv = _numeroAtividade;
    var count = 0;


    var scrollPosition = null // Trava SCROlL
    $(function () {

        $(".draggable").each(function () {
            $(this).data({
                'originalLeft': $(this).css('left'),
                'originalTop': $(this).css('top'),
            })
        })


        $(".draggable").draggable({
            revert: "invalid",
            drag: function () {

                $(this).removeClass("posicionado")
                $.scrollify.disable();
                // if (!scrollPosition)
                // scrollPosition = [
                //     self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
                //     self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
                // ];
                // var html = jQuery('html');
                // html.data('scroll-position', scrollPosition);
                // html.data('previous-overflow', html.css('overflow'));
                // html.css('overflow', 'hidden');
                // window.scrollTo(scrollPosition[0], scrollPosition[1]);
                // $("html").css("overflow", "hidden")
            },
            stop: function () {
                $.scrollify.enable();
                // scrollPosition = null;
                // var html = jQuery('html');
                // var scrollPosition = html.data('scroll-position');
                // html.css('overflow', html.data('previous-overflow'));
                // window.scrollTo(scrollPosition[0], scrollPosition[1])
                // $("html").css("overflow", "auto")
            }
        });

        $(".droppable").attr("dropado", false);

        $(".droppable").droppable({
            drop: function (event, ui) {
                let click = $(this).parent().parent().parent()
                if ($(this).parent().hasClass("1para1")) /*função para 1 para 1*/{
                    $(this).parent().children().each(function () {
                        //Verifica se já foi posicionado anteriormente e remove as classes anteriores
                        if ($(this).hasClass("droppable")) {
                            if ($(this).children().length == 0) {
                                $(this).removeClass("correto")
                                $(this).removeClass("posicionado")
                            }
                            if (count > 0) count--;
                        }
                    })

                    // verifica se a posição está vazia
                    if ($(this).children().length == 0) {
                        $(this).addClass("posicionado");

                        $(this).prepend($(ui.draggable));


                        centralizaDrop(ui, $(this)); // centraliza o objeto


                        $(this).parent().children().each(function () {
                            if ($(this).hasClass("posicionado"))
                                count++; // conta numero de posicionados
                        })
                        if ($(this).attr("class").substring(0, 6) == ui.draggable.attr("class").substring(0, 6)) {
                            $(this).addClass("correto") //se posicionado está correto
                        } else {
                            $(this).removeClass("correto") // se posicionado está errado
                        }

                        if (verificaTotal(this) == verificaVazio($(click)[0].id.substring($(click)[0].id.length - 1, $(click)[0].id.length))) { // verifica se ja finalizou o execicio
                            $(this).parent().children().each(function () {
                                if ($(this).hasClass("draggable")) { // desabilita o arraste
                                    $(this).draggable('disable')
                                }
                            })
                            var temp = 0;

                            if ($(this).parent().children().each(function () {
                                if ($(this).hasClass("correto")) //verifica a quantiodade de corretos
                                    temp++
                            }))
                                if (verificaTotal(this) == temp)
                                    resposta($(click)[0].id.substring($(click)[0].id.length - 1, $(click)[0].id.length), 0); // CORRETA
                                else {
                                    respostaErrada($(click)[0].id.substring($(click)[0].id.length - 1, $(click)[0].id.length), 1); // INCORRETA
                                    $(click).find(".droppable").each(function(){
                                       let elemento = $(this).children()
                                        $(click).find(".parentDraggable").each(function (){
                                            $(this).append(elemento)
                                            verificaVazio($(click)[0].id.substring($(click)[0].id.length - 1, $(click)[0].id.length))
                                        })
                                    })
                                }
                        }
                    } else {
                        $(ui.draggable).draggable({
                            revert: true
                        })
                    }
                } else /*função para 1paraN*/{
                    if ($(this).attr("class").substring(0, 6) == ui.draggable.attr("class").substring(0, 6)) {
                        $(this).prepend($(ui.draggable));
                        let completo = 1;

                        $(this).parent().parent().parent().find(".droppable").each(function(){
                            if ($(this).children().length != $(this).attr("data-limite")){
                                completo = 0;
                            }
                        })
                        if (completo) {
                            resposta($(click)[0].id.substring($(click)[0].id.length - 1, $(click)[0].id.length), 0); // CORRETA
                        }
                    } else {
                        respostaErrada($(click)[0].id.substring($(click)[0].id.length - 1, $(click)[0].id.length), 1); // INCORRETA
                        $(ui.draggable).draggable({
                            revert: true
                        })
                    }
                }
            }
        });

        function centralizaDrop(ui, th) {
            $(ui.draggable).css({
                top: /*th.position().top + (th.height()-$(ui.draggable).height())/2*/ 0,
                left: /*th.position().left + (th.width()-$(ui.draggable).width())/2*/ 0,
                width: "100%",
                height: "100%"
            });
        }

        function verificaTotal(th) {
            var temp = 0;
            $(th).parent().children().each(function () {
                if ($(this).hasClass("droppable"))
                    temp++
            });
            return temp;
        }

        function verificaVazio(_numeroAtv) {
            _qdade = document.getElementById("clicArraste-" + _numeroAtv).getElementsByClassName("droppable");
            _aux = _qdade.length;
            for (let i = 0; i < _qdade.length; i++) {
                if (_qdade[i].children.length == 0) {
                    _qdade[i].classList.remove("correto");
                    _qdade[i].classList.remove("posicionado");
                    _aux--;
                }
            }
            return _aux;

        }
    });
});
