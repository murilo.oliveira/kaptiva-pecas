
var playerInterativo1 = (function(player, variavelVideo) {

    $.ajax({
        type: "Get",
        url: "JSON/"+player+".json",
        dataType: "json",
        success: function(data) {
            json = data;
        },
        error: function(){
            alert("json not found");
        }
    });

    var json;
    var _player = $("#"+player+" video");
    var _divRespostas = $("#"+player+" div");
    var _videos;
    var _count = 1;
    var _controle = false;
    var _isFeedback = false;
    var _isCorreto = false;


    setTimeout(function () {
        _videos = json.Objeto.length - 1;
    },500)


    _player.on("timeupdate", function (event) {
        if (!_isFeedback) {
            if (_count <= _videos) {
                if (this.currentTime >= (this.duration - 2) && !_controle && json.Objeto[_count].video.respostas.length > 0) {
                    _isCorreto = false;
                    _controle = true;
                    let _track = _player.contents()[3]
                    $(_track).attr("src", "")
                    var botaoResposta
                    for (var i = 0; i < json.Objeto[_count].video.respostas.length; i++) {
                        botaoResposta = ""
                        botaoResposta += '<div class="btnOpcao" id="enviar" onclick="'+variavelVideo+'.verificaResposta(' + i + ')">\n'
                        botaoResposta += '<imgs src="' + json.Objeto[_count].video.respostas[i] + '"/>'
                        botaoResposta += '</div>\n'
                        $(_divRespostas).append(botaoResposta)
                        $(_divRespostas).css("display", "flex")
                    }
                    _player.get(0).pause()
                } else if (this.currentTime >= (this.duration - 2) && json.Objeto[_count].video.respostas.length == 0) {
                    _controle = true;
                    $(_divRespostas).empty()
                    var botaoResposta
                    botaoResposta = ""
                    botaoResposta += '<div class="btnRepetir" id="enviar" onclick="'+variavelVideo+'.repetirAtividade()">\n'
                    botaoResposta += '<div><p>Repetir atividade \t&#8634;</p></div>'
                    botaoResposta += '</div>\n'
                    $(_divRespostas).append(botaoResposta)
                    $(_divRespostas).css("display", "flex")
                }
            }
        }
        if (_controle) {
            _player.get(0).pause()
        }
    })
    _player.on("ended", function (event) {
        if (_isFeedback) {
            _isFeedback = false;
            console.log(_videos)
            console.log(_count)
            if (_isCorreto && _count < _videos+1) {
                let _source = _player.contents()[1]
                let _track = _player.contents()[3]
                $(_track).attr("src", "")
                $(_source).attr("src", json.Objeto[_count].video.url)
                $(_track).attr("src", json.Objeto[_count].video.vtt)
                _player.get(0).currentTime = 0
                _player.get(0).load()
                _player.get(0).play()
            } else if (!_isCorreto && _videos != _count){
                var botaoResposta
                botaoResposta = ""
                botaoResposta += '<div class="btnFeedback" id="enviar" onclick="'+variavelVideo+'.verificaFeedback(1)">\n'
                botaoResposta += '<div><p>Repetir \t&#8634;</p></div>'
                botaoResposta += '</div>\n'
                botaoResposta += '<div class="btnFeedback" id="enviar" onclick="' + variavelVideo + '.verificaFeedback(2)">\n'
                botaoResposta += '<div><p>Continuar \t&#8594;</p></div>'
                botaoResposta += '</div>\n'
                $(_divRespostas).append(botaoResposta)
                $(_divRespostas).css("display", "flex")
            } else {
                botaoResposta = ""
                botaoResposta += '<div class="btnFeedback" id="enviar" onclick="'+variavelVideo+'.repetirAtividade()">\n'
                botaoResposta += '<div><p>Repetir atividade \t&#8634;</p></div>'
                botaoResposta += '</div>\n'
                $(_divRespostas).append(botaoResposta)
                $(_divRespostas).css("display", "flex")
            }
        }
    })

    this.verificaResposta = function (numero) {
        _controle = false;
        $(_divRespostas).empty()
        $(_divRespostas).css("display", "none")
        let _source = _player.contents()[1]
        let _track = _player.contents()[3]
        _isFeedback = true;
        if (json.Objeto[_count].video.respostaCorreta == numero) {
            _isCorreto = true;
            $(_track).attr("src", "")
            $(_source).attr("src",json.Objeto[_count].video.feedback.positivo)
            $(_track).attr("src", json.Objeto[_count].video.feedbackVtt.positivo)
            _count++;
            _player.get(0).currentTime = 0
            _player.get(0).load()
            _player.get(0).play()
        } else {
            _isCorreto = false;
            $(_track).attr("src", "")
            $(_source).attr("src",json.Objeto[_count].video.feedback.negativo)
            $(_track).attr("src", json.Objeto[_count].video.feedbackVtt.negativo)
            _player.get(0).currentTime = 0
            _player.get(0).load()
            _player.get(0).play()

        }
    }

    this.verificaFeedback = function (numero) {
        _controle = false;
        $(_divRespostas).empty()
        $(_divRespostas).css("display", "none")
        let _source = _player.contents()[1]
        let _track = _player.contents()[3]
        if (2 == numero) {
            _count++;
        }
        $(_track).attr("src", "")
        $(_source).attr("src", json.Objeto[_count].video.url)
        $(_track).attr("src", json.Objeto[_count].video.vtt)
        _player.get(0).currentTime = 0
        _player.get(0).load()
        _player.get(0).play()
    }

    this.repetirAtividade = function () {
        _controle = false;
        $(_divRespostas).empty()
        $(_divRespostas).css("display", "none")
        let _source = _player.contents()[1]
        let _track = _player.contents()[3]
        _count = 1;
        $(_track).attr("src", "")
        $(_source).attr("src", json.Objeto[_count].video.url)
        $(_track).attr("src", json.Objeto[_count].video.vtt)
        _player.get(0).currentTime = 0
        _player.get(0).load()
        _player.get(0).play()
    }

});